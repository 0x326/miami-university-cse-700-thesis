\documentclass[12pt, oneside]{book2}

\newcommand{\documentTitle}{AN ARCHITECTURE FOR POLICY-AWARE INTENTIONAL AGENTS}
\newcommand{\documentType}{Thesis}
\newcommand{\documentAuthor}{John Maximilian Meyer}
\newcommand{\documentYear}{2021}
\newcommand{\documentKeywords}{intelligent agent; policy; logic programming}

\newcommand{\thesisAdvisor}{Daniela Inclezan, Ph. D}
\newcommand{\thesisFirstReader}{Alan Ferrenberg, Ph. D}
\newcommand{\thesisSecondReader}{Matthew Stephan, Ph. D}

% Adjust auto-generated headings
\newcommand\contentsnamebase{Table of Contents}
\newcommand\listfigurenamebase{List of Figures}
\newcommand\listtablenamebase{List of Tables}

\renewcommand\contentsname{\centering \large \contentsnamebase}
\renewcommand\listfigurename{\centering \large \listfigurenamebase}
\renewcommand\listtablename{\centering \large \listtablenamebase}

% Template imports
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amscd}
\usepackage{fullpage}
\usepackage{epstopdf}
\usepackage{appendix}
\usepackage{hyperref}
\usepackage{color}
\usepackage{subfig}
\usepackage{setspace}
\usepackage{tensor}
\usepackage{url}
\usepackage{verbatim}

% Custom imports

% Set document font family
\usepackage{fontspec}
\setmainfont{Times New Roman}

% Enable \enquote{} command
\usepackage{csquotes}

% enable graphics for images
\usepackage{graphicx}

\usepackage{bm}

\usepackage{amsthm}

\usepackage[capitalize,noabbrev]{cleveref}

\usepackage[super]{nth}

\usepackage[square,numbers]{natbib}

\usepackage{xcolor}

\usepackage{listings}
\usepackage{minted}

% \usepackage{rotating}
\usepackage{pdflscape}

\usepackage{framed}

\usepackage[normalem]{ulem}

\usepackage{tabularx}

\usepackage[type={CC},modifier={by},version={4.0}]{doclicense}

\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black,
    linktocpage,
    pdfpagemode=UseOutlines,
    pdfauthor=\documentAuthor,
    pdftitle=\documentTitle,
    pdfsubject={Computer Science \& Software Engineering},
    pdfkeywords=\documentKeywords,
    pdflang={en-US},
}

% Define image file extensions
\DeclareGraphicsExtensions{.pdf,.eps,.png,.jpg}

\theoremstyle{definition}
\newtheorem{definition}{Definition}
\crefname{definition}{Definition}{Defintions}

\newtheorem{lemma}{Lemma}
\crefname{lemma}{Definition}{Defintions}

\newtheorem{example}{Example}
\crefname{example}{Example}{Examples}

\newtheorem{example_scenario}{Example Scenario}
\crefname{example_scenario}{Example Scenario}{Example Scenarios}

\lstset{
    % backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
    basicstyle=\footnotesize,        % the size of the fonts that are used for the code
    breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
    breaklines=true,                 % sets automatic line breaking
    captionpos=t,                    % sets the caption-position to bottom
    commentstyle=\color{gray},       % comment style
    deletekeywords={...},            % if you want to delete keywords from the given language
    escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
    extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
    firstnumber=1,                   % start line enumeration with line 1000
    frame=single,                    % adds a frame around the code
    keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
    keywordstyle=\color{blue},       % keyword style
    language=Octave,                 % the language of the code
    morekeywords={*,...},            % if you want to add more keywords to the set
    numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
    numbersep=5pt,                   % how far the line-numbers are from the code
    numberstyle=\tiny\color{gray},   % the style that is used for the line-numbers
    rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
    showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
    showstringspaces=false,          % underline spaces within strings only
    showtabs=false,                  % show tabs within strings adding particular underscores
    stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
    stringstyle=\color{magenta},       % string literal style
    tabsize=2,                       % sets default tabsize to 2 spaces
    title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}
% See https://mirrors.rit.edu/CTAN/macros/latex/contrib/minted/minted.pdf
\setminted{
    linenos=true,
    samepage=true
}
\setminted[python]{
    python3=true
}

\onehalfspacing

\begin{document}
\thispagestyle{empty}
\singlespacing

\begin{center}

    ABSTRACT

    \vspace{\baselineskip}

    \documentTitle

    \vspace{2\baselineskip}

    by \documentAuthor

    \vspace{4\baselineskip}

\end{center}

\noindent
\input{1_Front_Matter/Abstract}

\newpage

\thispagestyle{empty}

\frontmatter

\doublespacing

\begin{titlepage}
\begin{center}

    \documentTitle

    \vspace{0.5\baselineskip}

    A \documentType \\

    \vspace{0.5\baselineskip}

    Submitted to the \\
    Faculty of Miami University \\
    in partial fulfillment of \\
    the requirements for the degree of \\
    Master of Science \\
    by \\
    \documentAuthor \\
    Miami University \\
    Oxford, Ohio \\
    \documentYear

    \vspace{1\baselineskip}

    Advisor: \thesisAdvisor\\
    Reader: \thesisFirstReader\\
    Reader: \thesisSecondReader\\

    \vspace{3\baselineskip}

    \textcopyright \documentYear{ }\documentAuthor

    % \doclicenseLongText
    %
    % \vspace{0.5cm}
    %
    % \doclicenseImage

    \newpage

    This Thesis titled

    \vspace{0.5\baselineskip}

    \documentTitle

    \vspace{1\baselineskip}

    by

    \vspace{1\baselineskip}

    \documentAuthor

    \vspace{1\baselineskip}

    has been approved for publication by\\
    \vspace{0.5\baselineskip}
    The College of Engineering and Computing\\
    and\\
    The Department of Computer Science \& Software Engineering\\

    \vspace{1.5cm}

    \rule[-0.1cm]{8cm}{0.01cm} \\
    \thesisAdvisor \\

    \vspace{1.0cm}

    \rule[-0.1cm]{8cm}{0.01cm} \\
    \thesisFirstReader \\

    \vspace{1.0cm}

    \rule[-0.1cm]{8cm}{0.01cm} \\
    \thesisSecondReader \\

\end{center}
\thispagestyle{empty}
\end{titlepage}

\singlespacing
\setcounter{page}{3}

\tableofcontents
\setcounter{tocdepth}{2}
\cleardoublepage
% \phantomsection

\addcontentsline{toc}{chapter}{\listtablenamebase}
\listoftables
\cleardoublepage

\addcontentsline{toc}{chapter}{\listfigurenamebase}
\listoffigures

\newpage

% \include{2_Chapters/Chapter00_Acknowledgements}

\mainmatter

\include{2_Chapters/Chapter01_Introduction}
\include{2_Chapters/Chapter02_Background}
\include{2_Chapters/Chapter03_APIA}
\include{2_Chapters/Chapter04_Implementations}
\include{2_Chapters/Chapter05_Benchmark_Setup}
\include{2_Chapters/Chapter06_Evaluation}
\include{2_Chapters/Chapter07_Conclusion}

\appendix
\include{3_Appendices/A1_APL}
\include{3_Appendices/A2_AOPL}
\include{3_Appendices/A3_PDC-agent}
\include{3_Appendices/A4_APIA}

\backmatter

\bibliographystyle{unsrtnat}
\addcontentsline{toc}{chapter}{References}
\bibliography{References}
\end{document}
