[OhioLink]: http://rave.ohiolink.edu/etdc/view?acc_num=miami1619199523368049
[APIA]: https://gitlab.com/0x326/miami-university-cse-700-apia.git

# An Architecture for Policy-Aware Intentional Agents by John Meyer

Published to [OhioLink].

The implementation of the APIA architecture is available [here][APIA].

## Prerequisites

```bash
... # Build clingo v5.4.1 from source with Python 3.9 support

yarn install
```

## Usage

```bash
make
```
