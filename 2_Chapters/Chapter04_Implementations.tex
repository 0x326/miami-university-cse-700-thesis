\chapter{Implementations}

% Use this chapter to discuss the main content of your thesis contributions.
%
% You may have several of these chapters depending on your thesis - work with your advisor to determine chapter layouts.
% Remove or add as necessary.

\section{Update to $\mathcal{AIA}$}

\subsection{CR-Prolog to ASP-Core-2}

Since $\mathcal{APIA}$ takes $\mathcal{AIA}$ as a basis, we first update the dependencies of \citet{blount_architecture_2013,blount_towards_2014}'s $\mathcal{AIA}$ implementation such that it requires a state-of-the-art solver\footnotemark.
Specifically, we update his $\mathcal{AIA}$ implementation from \textsc{CRModels2} (version 2.0.15)~\citep{balduccini_crmodels2_2015} to \textsc{clingo} (version 5.4.1)~\citep{roland_kaminski_potasscoclingo_2020}.
This is a non-trivial change since \textsc{CRModels2} uses a derivative of the ASP language called \textit{CR-Prolog} while \textsc{clingo} uses a standardized version of ASP called \textit{ASP-Core-2}~\citep{calimeri_asp-core-2_2020}.

\footnotetext{
    Our update to \citet{blount_architecture_2013,blount_towards_2014}'s $\mathcal{AIA}$ implementation is available at \url{https://gitlab.com/0x326/miami-university-cse-700-aia.git} and is released under the MIT open-source license.
}

Consistency-Restoring Prolog (CR-Prolog) is an extension of ASP that adds a new kind of statement: \textit{consistency-restoring rules} (cr-rules).
A CR-rule with label $r$:
\begin{equation}
    r: head \stackrel{+}\leftarrow body.
\end{equation}
works like a normal ASP rule except that, given the body, the head is only generated when it is absolutely necessary (i.e.~when the logic program would be unsatisfiable without it and there are no other non-cr-rule alternatives).
Hence, the application of this rule \textit{restores} consistency to the logic program.

CR-Prolog also assigns a special meaning to the $prefer(r_1, r_2)$ predicate.
Given $prefer(r_1, r_2)$ the application of cr-rule $r_1$ will be preferred to the application of cr-rule $r_2$, though either might restore consistency.

While the translation of CR-Prolog into ASP has been addressed before by \citet{lee_translating_2018,yang_translating_2018}, their approach is long and complex.
We introduce a much simpler approach that achieves the same effect.
Given a cr-rule with label $r$:
\begin{equation}
    r: head \stackrel{+}\leftarrow body.
\end{equation}
we translate this into ASP-Core-2 by writing:
\begin{gather}
    apply\_cr\_rule(r) \leftarrow body. \\
    head \leftarrow apply\_cr\_rule(r). \\
    \label{eq:apia_cr_prolog_prefer_weak_constraint}
    \text{:}\sim apply\_cr\_rule(r). [1@cr_1, apply\_cr\_rule(r)]
\end{gather}

\Cref{eq:apia_cr_prolog_prefer_weak_constraint} is a weak constraint.
It informs the ASP solver that applying cr-rule $r$ incurs a cost of 1.
Should there exist another answer set where this rule is not applied, the use of the cr-rule makes the answer set suboptimal and an optimizing solver will not choose it.

Likewise for prefer predicates, we translate the following CR-Prolog:
\begin{equation}
    prefer(r_1, r_2) \leftarrow body.
\end{equation}
into the following ASP-Core-2 syntax:
\begin{gather}
    cr\_prefer(r_1, r_2) \leftarrow body. \\
    \begin{split}
        \text{:}\sim \
            & cr\_prefer(r_1, r_2), \\
            & apply\_cr\_rule(r_2), \\
            & \textbf{not } apply\_cr\_rule(r_1). \\
            & [1@cr_2, apply\_cr\_rule(r_2), apply\_cr\_rule(r_1)]
    \end{split}
\end{gather}

This weak constraint informs the solver that applying cr-rule $r_2$ over cr-rule $r_1$ incurs a cost of 1 (since $r_1$ is preferred over $r_2$).
Should another answer set exist where this preference is honored, then this answer set is suboptimal and an optimizing solver will not choose it.

Since the semantics of prefer takes precedence over the semantics of cr-rules, our weak constraints have priorities $cr_2$ and $cr_1$ where $cr_2$ > $cr_1$.

In order to configure \textsc{clingo} to optimize, we pass the \texttt{--opt-mode=optN} commandline parameter.

\subsection{Control Loop}

In addition to upgrading the $\mathcal{AIA}$ logic program, we also refactor the $\mathcal{AIA}$ control loop.
In his dissertation, \citet{blount_architecture_2013,blount_towards_2014} introduced the $\mathcal{AIA}$ Agent Manager.
This is an interactive Java program that allows an end-user to assign values to agent observations in a graphical interface for each control loop iteration.
Since this requires manual input, it does not easily lend itself to automation and reproducibility of execution, which are required for performance benchmarking.

Furthermore, the $\mathcal{AIA}$ Agent Manager~\citep{blount_architecture_2013} is structured around interacting with an underlying solver using subprocesses and process pipes.
While the $\mathcal{AIA}$ Agent Manager could conceivably invoke \textsc{clingo} as a subprocess, \textsc{clingo} 5 provides a unique opportunity for more advanced integrations using its Python API.

Because of these two points, we replace the $\mathcal{AIA}$ Agent Manager with a new implementation of the $\mathcal{AIA}$ control loop written in Python 3.9.0.
This new implementation uses a command-line interface and allows for reproducible execution through ASP input files.
Since this control loop is also the basis for our $\mathcal{APIA}$ implementation, we will discuss it more in \cref{subsec:apia_control_loop}.

\section{$\mathcal{APIA}$}

\subsection{$\mathcal{AIA}$ Logic Program}

For our implementation of the $\mathcal{APIA}$ architecture, we re-implement the $\mathcal{AIA}$ logic program in ASP using only the description of the architecture presented by \citet{blount_architecture_2013,blount_towards_2014} and our approach to CR-Prolog translation that we propose in the previous section\footnotemark.
While the majority of our ASP rules are almost identical to \citet{blount_architecture_2013,blount_towards_2014}'s implementation as a consequence, this approach allows us to make syntactic, semantic, and organizational changes where we see fit.

\footnotetext{
    Our $\mathcal{APIA}$ implementation is available at \url{https://gitlab.com/0x326/miami-university-cse-700-apia.git} and is released under the MIT open-source license.
}

One such set of changes concerns rules about agent histories and the interpretation of unexpected exogenous actions.
We introduce a cr-prefer statement so that, in the case of multiple possible explanations for an unexpected observation, the agent prefers explanations with recent occurrences of exogenous actions.
This is because the occurrence of an exogenous action changes what the agent believes to be true at each timestep.
For example, if an exogenous action is thought to have occurred at timestep 0, then its effect will be propagated to every subsequent timestep by inertia.
On the other hand, a recent exogenous action leaves most of our agent's prior knowledge unchanged and primarily affects current and future values of fluents.
Since the latter typically requires a smaller change to the agent's knowledge base, such an explanation is preferred.
This has an added benefit of making the $\mathcal{AIA}$ control loop deterministic.

We also restructure the collection of ASP rules into ASP subprograms.
This allows future work to easily investigate applying incremental grounding to the $\mathcal{AIA}$ control loop.
This change also simplifies the $\mathcal{AIA}$ intended action rules since the presence of the $interpretation(x, n)$ predicate is no longer needed to differentiate step 1 and step 2 of the control loop.

We also make minor modifications to $\mathcal{AIA}$ as a whole.
First, we refactor the arrangement of ASP rules into multiple files according to their purpose in the $\mathcal{AIA}$ architecture (e.g.~whether they are part of $\mathcal{TI}$, $\mathcal{AIA}$'s rules for computing models of history, or the $\mathcal{AIA}$ intended action rules)
Second, we refactor the names of mental fluents in the Theory of Intentions so that their names are more descriptive and self-documenting.
Thirdly, we extensively add inline comments to each ASP rule with reference quotations and page numbers from \citet{blount_architecture_2013,blount_towards_2014}.
Lastly, we make minor modifications to rules in $\Pi(\Gamma_n)$ so that they are valid according to the mathematical definitions proposed by \citet{blount_architecture_2013,blount_towards_2014}.

\subsection{Python Component}
\label{subsec:apia_control_loop}

We provide an implementation of the $\mathcal{AIA}$ control loop for the $\mathcal{APIA}$ architecture.
The $\mathcal{AIA}$ control loop is implemented using Python 3.9.0 and \textsc{clingo} 5.4.1 using \textsc{clingo}'s Python API.
We provide two modes: an automatic mode and a manual mode.
The automatic mode is intended to be used for normal execution while the manual mode is intended to aid in debugging unexpected output in answer sets.

The automatic mode uses a command-line interface to specify the ASP files of the input domain, the observations of the agent, and the $\mathcal{APIA}$ mode the agent should use.
The control loop then provides human-readable output as to what happens at each control loop step (See \cref{fig:apia_example_a_execution_paraniod_subordinate}).

\begin{figure}[h]
    \centering
    \lstinputlisting[basicstyle=\scriptsize]{Figures/APIA_Architecture/Examples/Authorization/Example_A/Snippets/Example_Execution.snippet.txt}
    \caption{Automatic execution of Example A using configuration $(paranoid, subordinate)$}
    \label{fig:apia_example_a_execution_paraniod_subordinate}
\end{figure}

In the case of unexpected output, the manual mode allows one to examine the answer set at each step of the control loop.
It also provides scripts to highlight differences between answer sets of different timesteps in a visual manner (see \cref{fig:apia_diff_test}) and to step through the control loop like one would do in a traditional debugger.

Manual mode also assists in catching particular programming mistakes by introducing \\ $invalid(P, R) $predicates, where $P$ is an invalid predicate and $R$ is a human-readable explanation as to why.
For example, it can detect some cases of typos in action descriptions by looking for a $holds(F, I)$ predicate where $F$ is not defined as a fluent.
Similarly, with $occurs(A, I)$.

It also addresses certain violations of $\mathcal{AIA}$ and $\mathcal{AOPL}$ underlying assumptions.
For example, it generates an invalid predicate when there exists an action that is neither a physical, mental, or policy action.
Likewise when an action is neither an agent action nor an exogenous action.
In addition, it generates an invalid predicate when an $\mathcal{AOPL}$ policy statement describes an object that is not registered as an action.

These rules have been very useful in debugging the implementation of the $\mathcal{APIA}$ architecture and they will aid future end-users who encode and execute scenarios using this architecture.
Since these rules are intended during debugging, they are not executed during the automatic mode.
