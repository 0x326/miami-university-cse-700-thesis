\chapter{Conclusion}

% Present the following:
%
% \begin{itemize}
%     \item summary of your work
%     \item list of contributions
%     \item limitations and drawbacks to your approach
%     \item future work
%     \item other important concluding details
% \end{itemize}
%
% Also, here's the Miami Logo to demonstrate including images in your thesis.
% The logo is seen in Figure~\ref{fig:MiamiLogo}.
%
% \begin{figure}[h]
%     \centering
%     % \includegraphics[width=0.4\textwidth]{Figures/Miami}
%     \caption{The Miami University Logo}
%     \label{fig:MiamiLogo}
% \end{figure}
%
% \section{Future Work}

In this thesis, we have surveyed different agent architectures ranging from production rule systems, logic-based architectures (such as BDI architectures), and those with normative models and cognitive models.
The focus of this work has been on modern logic-based architectures.
We began by introducing transitions systems, which model the agent's environment like a directed graph of states.
Then we discussed the development of a formal syntax that specifies transition systems.
Next, we introduced the AAA agent architecture.
While the AAA architecture is goal-oriented and uses information encoded in an action language to achieve them, it does not persist a plan across iterations of its control loop.
\Citet{blount_architecture_2013,blount_towards_2014} deem this a limitation and prompts the creation of the $\mathcal{AIA}$ architecture.

The $\mathcal{AIA}$ architecture represents an agent's mental state as properties of the world that are internal to the agent.
The $\mathcal{AIA}$ architecture then uses this mental state to persist an agent's plans (which are encapsulated in a construct called an activity) as well as its current progress in executing them.
The agent's mental state is maintained by a set of logic rules called the Theory of Intentions ($\mathcal{TI}$).
We then described the $\mathcal{AIA}$ control loop and explained its execution through the aid of a few examples.

We introduced languages $\mathcal{APL}$ and $\mathcal{AOPL}$ to represent and reason over action compliance given authorization and obligation policies.
While $\mathcal{APL}$ and $\mathcal{AOPL}$ are not action languages, they share a similar syntax to that of action language $\mathcal{AL}$ and work in tandem with it.

We then contrasted the $\mathcal{AIA}$ agent architecture and $\mathcal{AOPL}$ to the PDC-agent architecture.
One feature of the PDC-agent architecture is that it supports multi-agent cooperation.
PDC-agent also shares a similar mechanism to $\mathcal{TI}$ for updating beliefs via internal actions.
However, the PDC-agent architecture requires enumerating plans by hand and annotating which goals they achieve.
Since the $\mathcal{AIA}$ architecture is based on action languages, plans can be automatically generated, and their respective end-goal can be computed.

We extended the $\mathcal{AIA}$ architecture so that it reasons over $\mathcal{AOPL}$ policies.
This new agent architecture is called the \textit{Architecture for Policy-Aware Intentional Agents} ($\mathcal{APIA}$).
As part of this contribution, we re-envisioned the notion of $\mathcal{AOPL}$ compliance such that only future agent actions affect policy compliance (excluding past and exogenous actions).
This allowed us to represent a notion of ``turning a new leaf'' regarding policy compliance after performing non-compliant actions in the past.
We also updated the interaction of authorization and obligation policy statements by updating the logic translation of defeasible statements and adding a non-contradiction axiom.

Using these changes to $\mathcal{AOPL}$, we invented a series of \textit{policy fluents} to represent $\mathcal{AOPL}$ compliance in terms that $\mathcal{AIA}$ can reason over.
Ultimately, we introduced a defined fluent $policy\_compliant(f)$ that, when set as the agent's goal, can only be achieved through policy compliant means (i.e.~when the physical fluent $f$ is achieved through an activity that is $l$-compliant, for some threshold $l$).
However, to handle scenarios where no $l$-compliant activity exists, we introduced a series of \textit{policy actions} to ignore the effect of less-than-$l$-compliant actions on policy compliance.
We then parameterized the use of policy actions such that we introduced agents with varying degrees of policy adherence.
For example, we introduced a $(paranoid, subordinate)$ agent that refuses to deviate from its policy, a $(best \ effort, best \ effort)$ agent that tries to adhere to its policy, and a $(utilitarian, utilitarian)$ agent that completely ignores its policy.
We demonstrated the execution of these $\mathcal{APIA}$ agents in the context of several examples, ranging from fortunate to worst case.

To implement the $\mathcal{APIA}$ architecture, we first refactored the original $\mathcal{AIA}$ implementation provided by \citet{blount_architecture_2013,blount_towards_2014} to use state-of-the-art programming tools.
To accomplish this, we invented a translation of CR-Prolog to ASP-Core-2 as well as implemented a new automated control loop in Python (to replace $\mathcal{AIA}$'s original Java GUI).
We then used these contributions as a basis for our $\mathcal{APIA}$ implementation.

We evaluated the $\mathcal{APIA}$ architecture in terms of performance, conciseness, expressivity, elaboration tolerance, and difficulty of construction.
On the basis of 100 trials, the difference in elapsed time and CPU time between all modes that we considered was statistically significant.
The differences between the $(best \ effort, best \ effort)$ mode and the other modes were the most statistically significant (although these increases were less than 1 second).
$\mathcal{APIA}$ provides a very concise, expressive, and elaboration tolerant approach to representing knowledge.
While action language $\mathcal{AL}$ and language $\mathcal{AOPL}$ do have some expressivity limitations, we overcame these by utilizing the underlying expressivity of ASP, to which both of these languages are translated for execution.

To summarize, the $\mathcal{APIA}$ agent architecture is a modern logic-based architecture that is able to plan amid changing environments while maintaining policy compliance.
As a result, its behavior is both explainable and ensured to be compliant.
With such properties, the $\mathcal{APIA}$ architecture is a significant contribution to the logic programming community.

\section{Future Work}

The $\mathcal{APIA}$ architecture presented in this thesis can be extended in the following ways:

\begin{itemize}
    \item Currently, the entire logic program is re-run for each step of the $\mathcal{AIA}$ control loop.
        This means every rule has to be re-applied each time \textsc{clingo} is run.
        Exploring ASP \textit{incremental grounding} (or \textit{multi-shot solving})~\citep{gebser_potassco_2019} could provide a significant performance boost, as this would allow most derived predicates and facts to be re-used in subsequent \textsc{clingo} invocations.
        The use of \textsc{clingo}'s \texttt{\#external} statement may be useful in this effort.
\end{itemize}
