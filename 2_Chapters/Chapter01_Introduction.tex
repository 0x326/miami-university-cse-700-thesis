\chapter{Introduction}

% Use this chapter to introduce readers to your research.
%
% Discuss motivations, contributions and other introductory material.
%
% \textbf{Note:}
% In general, the suggestions provided in this template (for content, chapter breakdown, etc.) are just that - suggestions.
% Consult with your advisor to determine the appropriate thesis structure for your sub-discipline, and adjust accordingly.

Computers have increasingly become a ubiquitous part of everyday life.
As more intelligence is able to be embedded in them, we are starting to see the rise in autonomous systems.
Automatic stock trading programs and self-driving cars are but a few examples.
However, as we expect them to perform more complex tasks, the question arises of how to best construct agents in a computer program that are capable of reasoning over higher-level concepts.
Furthermore, new government regulations can place requirements on how agents must act.
Thus, designers must ensure the lawful behavior of their agents, which further complicates their design.

This thesis provides a background on different \textit{agent architectures} that have been proposed in prior literature.
This work then narrows its focus to discuss a particular class of agent architectures that are heavily based in the formal theory of logic.
It first introduces preliminary definitions on which they are based, then moves on to discuss them in significant detail with the aid of examples.
Then, this paper contrasts them to alternative agent architectures.
Lastly, we move to describe the contribution of this thesis and evaluate it.

\section{Motivation}

Policy compliance has become an important aspect of Artificial Intelligence in recent years due to the signing of the General Data Protection Regulation (GDPR) in Europe.
The GDPR establishes standards for gathering user data as well as for making automated decisions upon it.
Among other requirements, the GDPR stipulates that users must provide consent before any data can be used and that all actions of automated processes using the data must be explainable to a third-party~\citep{sandra_wachter_artificial_2018}.
Furthermore, the GDPR requires businesses to prove their compliance with its statues~\citep{sandra_wachter_artificial_2018}.

As machine learning approaches fail to provide a human-understandable explanation for their output, an alternative approach is needed.
Logic-based approaches are inherently better in this regard since they are logically deterministic by their construction.
Likewise, logic-based approaches provide benefits in the area of assured compliance as actions can be proven to be compliant.

However, prior work in the logic community has yet to integrate work on action compliance into a non-trivial agent architecture.
Most work regarding policy compliance is limited to access control policies regarding allowing or denying access to an Internet system.
Such an access control enforcer is only a very trivial agent that is only given two simple actions to perform (to allow and to deny).
Prior work has proposed various agent architectures that are more complex than these simple agents.
Because of their capability to perform more complex actions in changing environments, the research community ought to consider applying to them more complex policies than access control policies.

Thus, this thesis combines the approach of prior research on complex authorization and obligation policies with that of research on non-trivial agent architectures.
This thesis creates a derivative agent architecture that is capable of reasoning over and ensuring action compliance.
We evaluate the architecture against prototypical examples that, while simplified in contrast to the real-world, are nonetheless non-trivial in complexity.

\section{Contributions}

The contributions of this thesis are summarized as follows:

\begin{itemize}
    \item Created examples on which a policy-aware agent architecture can be evaluated.
    \item Created a new extension of an agent architecture that incorporates research on policy compliance.
    \item Implemented the new agent architecture using state-of-the-art logic programming tools.
    \item Applied and evaluated the new agent architecture against the created examples.
    \item Evaluated the runtime efficiency of the new agent architecture in different configuration modes.
    \item Refactored a prior agent architecture implementation to state-of-the-art logic programming tools.
\end{itemize}
