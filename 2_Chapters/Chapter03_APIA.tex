\chapter{The $\mathcal{APIA}$ Architecture}

% Use this chapter to discuss the main content of your thesis contributions.
%
% You may have several of these chapters depending on your thesis - work with your advisor to determine chapter layouts.
% Remove or add as necessary.
%
% \section{Sub-Topic 1}
%
% \subsection{Sub-Sub-Topic I}
%
% \subsection{Sub-Sub-Topic II}
%
% \section{Sub-Topic 2}
%
% \section{Sub-Topic 3}

In this chapter, we present the $\mathcal{APIA}$ architecture.
The Architecture for Policy-Aware Intentional Agents ($\mathcal{APIA}$) is an agent architecture heavily inspired by the $\mathcal{AIA}$ agent architecture that reasons over $\mathcal{AOPL}$ policies.
Like the $\mathcal{AIA}$ architecture, the $\mathcal{APIA}$ architecture requires the environment to be represented in a propositional transition system and has the same applicability conditions.

\section{Re-envisioning $\mathcal{AOPL}$ policies in an agent-centered architecture}

\Citet{gelfond_authorization_2008} conceived $\mathcal{AOPL}$ as a means to evaluate the history of the world at the end of the day.
This differs from $\mathcal{AIA}$ in the following ways:

\begin{itemize}
    \item $\mathcal{AOPL}$ evaluates histories of actions from the perspective of the world whereas $\mathcal{AIA}$ distinguishes between agent actions and exogenous actions.
    \item $\mathcal{AOPL}$ evaluates histories at ``the end of the day'' whereas the $\mathcal{AIA}$ architecture, while it does reason over past actions in diagnosis, places an emphasis on planning future actions to achieve a future state.
\end{itemize}

These differences prevent $\mathcal{AOPL}$ policies from interoperating with the $\mathcal{AIA}$ agent architecture out of the box.
To address the first issue, we require $\mathcal{AOPL}$ policies to describe only agent actions (the sets of agent actions and exogenous actions are disjoint).
We enforce this rule using a constraint.
For the second issue, we adjust our policy compliance rules such that only future actions affect the compliance of the world.
Since the focus of $\mathcal{AIA}$ is planning, past actions are always considered ``compliant'' although they might not have been at the time.
For an agent that previously had no choice but a non-compliant action, this allows the agent to conceive of ``turning a new leaf'' and seeking policy-compliant actions in the future.
Without this provision, it would be impossible for such an agent to achieve its goal in a policy compliant manner and a single non-compliant action would be non-recoverable.

Also, \citet{gelfond_authorization_2008}'s discussion on $\mathcal{AOPL}$ does not include how authorization policy statements interact with obligation policy statements.
For example, consider the following $\mathcal{AOPL}$ policy:
\begin{gather}
    permitted(a) \\
    obl(\neg a)
\end{gather}
and
\begin{gather}
    \neg permitted(a) \\
    obl(a)
\end{gather}
such policies seem to be contradictory.
In the first case, an agent is permitted to perform action $a$ but at the same time is obligated to refrain from it.
Appealing to common sense, if an agent is obligated to refrain from an action, one would conclude that the action is not permitted.
Likewise, it makes sense to say that if an agent is obligated to do an action, then it must be permitted.
Thus, we take these intuitions and create the following non-contradiction axioms:
\begin{gather}
    \begin{split}
        \leftarrow \
            & action(A), \\
            & obl(A, I), \\
            & \neg permitted(A, I).
    \end{split} \\
    \begin{split}
        \leftarrow \
            & action(A), \\
            & obl(neg(A), I), \\
            & permitted(A, I).
    \end{split}
\end{gather}
These enforce that, at the very least, the authorization and obligation policies do not contradict each other.
See \cref{table:apia_non_contradiction}.
Note, to allow for defeasible policy statements to work correctly, we do not write:
\begin{gather}
    \begin{split}
        permitted(A, I) \leftarrow \
            & action(A), \\
            & obl(A, I).
    \end{split} \\
    \begin{split}
        \neg permitted(A, I) \leftarrow \
            & action(A), \\
            & obl(neg(A), I).
    \end{split} \\
    \begin{split}
        \neg obl(A, I) \leftarrow \
            & action(A), \\
            & \neg permitted(A, I).
    \end{split} \\
    \begin{split}
        \neg obl(neg(A), I) \leftarrow \
            & action(A), \\
            & permitted(A, I).
    \end{split}
\end{gather}

\begin{table}[h]
    \centering
    \begin{tabular}{ | l | c | c | c | }
        \hline
        & $permitted(a)$ & $\neg permitted(a)$ & (none) \\
        \hline
        $obl(a)$ & Valid & \sout{(Contradictory)} & Valid \\
        \hline
        $obl(\neg a)$ & \sout{(Contradictory)} & Valid & Valid \\
        \hline
        (none) & Valid & Valid & Valid \\
        \hline
    \end{tabular}
    \caption{Non-contradictory combinations of $\mathcal{AOPL}$ policy statements}
    \label{table:apia_non_contradiction}
\end{table}

We also extend the translation of defeasible policy statements to ASP.
Suppose we have the following policy:
\begin{gather}
    \textbf{ normally } permitted(a) \\
    obl(\neg a) \textbf{ if } cond
\end{gather}
Using the approach proposed by \citet{gelfond_authorization_2008}, the corresponding ASP translation would be
\begin{gather}
    permitted(a, I) \leftarrow \textbf{not } \neg permitted(a, I). \\
    obl(neg(a), I) \leftarrow lp(cond).
\end{gather}

Given $cond$, at a given timestep we have $permitted(a)$ and $obl(\neg a)$.
This violates the non-contradiction axioms we introduced above.
So, we extend their approach by using the following encoding for the defeasible statement:
\begin{equation}
\begin{split}
    permitted(a, I) \leftarrow \
        & \textbf{not } \neg permitted(a, I), \\
        & \textbf{not } obl(neg(a), I).
\end{split}
\end{equation}
This allows the presence of $obl(\neg a)$ to be an exceptional case to the defeasible rule.

In general, we propose the following encodings for the following defeasible statements, respectively:
\begin{gather}
    d: \textbf{ normally } permitted(a) \textbf{ if } cond \\
    d: \textbf{ normally } \neg permitted(a) \textbf{ if } cond \\
    d: \textbf{ normally } obl(a) \textbf{ if } cond \\
    d: \textbf{ normally } obl(\neg a) \textbf{ if } cond
\end{gather}
\begin{gather}
\begin{split}
    permitted(a, I) \leftarrow \
        & lp(cond), \\
        & \textbf{not } abnormal(d, I), \\
        & \textbf{not } \neg permitted(a, I), \\
        & \textbf{not } obl(neg(a), I).
\end{split} \\
\begin{split}
    \neg permitted(a, I) \leftarrow \
        & lp(cond), \\
        & \textbf{not } abnormal(d, I), \\
        & \textbf{not } permitted(a, I), \\
        & \textbf{not } obl(a, I).
\end{split} \\
\begin{split}
    obl(a, I) \leftarrow \
        & lp(cond), \\
        & \textbf{not } abnormal(d, I), \\
        & \textbf{not } \neg obl(a, I), \\
        & \textbf{not } \neg permitted(a, I).
\end{split} \\
\begin{split}
    obl(neg(a), I) \leftarrow \
        & lp(cond), \\
        & \textbf{not } abnormal(d, I), \\
        & \textbf{not } obl(neg(a), I), \\
        & \textbf{not } permitted(a, I).
\end{split}
\end{gather}

\section{Policy-Aware Agent Behavior}

The $\mathcal{APIA}$ architecture takes the $\mathcal{AIA}$ architecture as a basis.

$\mathcal{AIA}$ describes the transition system in terms of fluents, actions, and axioms (i.e.~action descriptions).
For convenience, we differentiate fluents by category.
There exist physical fluents (which are those that describe the physical world), mental fluents (those introduced in the Theory of Intentions), and policy fluents (those that we introduce to reason over policy compliance).
Likewise, we extend \citet{blount_architecture_2013,blount_towards_2014}'s distinction of physical actions and mental actions by adding a third category: policy actions.

To reason over policy compliance, we invent a series of policy fluents, policy actions, and action descriptions (See \cref{fig:apia_list_policy_fluents_actions}).
These new action descriptions encode the effect of future agent actions on policy compliance and provide a means by which the $\mathcal{AIA}$ control loop can determine non-compliant activities to be futile and execute compliant ones in their place.
For example, consider dynamic causal laws such as the following:
\begin{gather}
    \label{eq:apia_auth_compliance_strong}
    a \textbf{ causes } \neg auth\_compliance(strong) \textbf{ if not } permitted(a) \\
    \label{eq:apia_auth_compliance_weak}
    a \textbf{ causes } \neg auth\_compliance(weak) \textbf{ if } \neg permitted(a)
\end{gather}

Given inertial policy fluents $auth\_compliance(weak)$ and $auth\_compliance(strong)$, \cref{eq:apia_auth_compliance_strong,eq:apia_auth_compliance_weak} correspond to the definition of authorization compliance of $\mathcal{AOPL}$ given in \cref{appendix:apl_compliance,appendix:aopl_compliance}.
Should there exist an action $a$ where $permitted(a)$ is not known to be true, then the scenario ceases to be strongly compliant (i.e.~it becomes weakly compliant).
Since no action can make $auth\_compliance(strong)$ true again, the rest of the scenario remains weakly compliant by inertia.
Likewise, should there exist an action $a$ where $permitted(a)$ is false, then the scenario ceases to be weakly compliant (i.e.~it becomes non-compliant) and remains in this state by inertia.
Similar rules to \cref{eq:apia_auth_compliance_strong,eq:apia_auth_compliance_weak} are added for every physical action $a$ of the transition system.

\begin{figure}[p]
    \begin{framed}
        Inertial Policy Fluents:
        \begin{itemize}
            \item $auth\_compliance(strong)$
            \item $auth\_compliance(weak)$
            \item $obl\_compliant(do\_action)$
            \item $obl\_compliant(refrain\_from\_action)$
        \end{itemize}

        Defined Policy Fluents:
        \begin{itemize}
            \item $policy\_compliant(f)$, for all physical fluents $f$
        \end{itemize}

        Policy actions:
        \begin{itemize}
            \item $ignore\_not\_permitted(a)$
            \item $ignore\_neg\_permitted(a)$
            \item $ignore\_obl(a)$
            \item $ignore\_obl(neg(a))$
        \end{itemize}
        for all physical actions $a$
    \end{framed}
    \caption{List of policy fluent and actions in the $\mathcal{APIA}$ architecture}
    \label{fig:apia_list_policy_fluents_actions}
\end{figure}

To inform the $\mathcal{AIA}$ control loop to achieve the agent's goal in a policy-compliant manner, we utilize defined fluents.
Since an agent in the $\mathcal{AIA}$ architecture can only select a single fluent as a goal, defined fluents serve as a way to combine fluents together using logical ANDs and logical ORs.
For example:
\begin{gather}
    c \textbf{ if } a, b \\
    d \textbf{ if } a \\
    d \textbf{ if } b
\end{gather}

Fluent $c$ is equivalent to $(a \land b)$ and $d$ is equivalent to $(a \lor b)$.
Using this mechanism, we introduce a defined policy fluent $policy\_compliant(f)$, where $f$ is a physical fluent.
The policy fluent $policy\_compliant(f)$ is true iff $f$ is true and $auth\_compliance(l)$ is true, for some minimum compliance threshold $l$.
Thus, when $policy\_compliant(f)$ is an agent's goal, activities below $l$-compliance are deemed as futile and the agent works to achieve fluent $f$ subject $l$-compliance.

To allow for the case when no activities that achieve $f$ are $l$-compliant or for when an agent deliberately chooses to act without $l$-compliance, we introduce policy actions $ignore\_not\_permitted(a)$ and $ignore\_neg\_permitted(a)$, where $a$ is a physical action.
By concurrently executing these actions with action $a$, our agent ignores the effect the event $<s, \{a\}>$ has on weak compliance or non-compliance, respectively.
These allow our agent to decide when it will obey its policy or disregard it.

One can imagine that agents could use this capability in multiple ways.
Some agents could strictly adhere to their policy (and hence, never use these actions) while others could be more utilitarian (and use them more liberally).
To this end, we have parameterized the agent's adherence to its policy (See \cref{table:apia_authorization_modes,table:apia_obligation_modes}).

One option for this parameter causes the agent to strictly adhere to its policy such that it never performs $ignore\_neg\_permitted(a)$.
This causes all non-compliant actions to indirectly cause $policy\_compliant(f)$ to be false.
Hence, only activities with weakly or strongly compliant actions are considered.
Since this mode never dares to become non-compliant, it is called \textit{subordinate}.

A similar option causes the agent to never perform $ignore\_not\_permitted(a)$.
This causes all weak and non-compliant actions to indirectly cause $policy\_compliant(f)$ to be false.
Hence, only activities with strongly compliant actions are considered.
Since weakly compliant actions are actions for which the policy compliance is unknown, this mode is called \textit{paranoid} as it treats weakly compliant actions as if they are non-compliant.

Another option allows unrestricted access to the $ignore\_neg\_permitted(a)$ and $ignore\_not\_permitted(a)$ actions.
This mode is called \textit{utilitarian} because it reduces the behavior of $\mathcal{APIA}$ to that of $\mathcal{AIA}$, where policies are not considered at all.

An interesting feature of the $ignore\_neg\_permitted(a)$ and $ignore\_not\_permitted(a)$ actions is the ability to optimize compliance.
Using preference statements in ASP, we can require the $\mathcal{AIA}$ control loop to prefer other actions over the use of $ignore\_not\_permitted(a)$ and $ignore\_neg\_permitted(a)$.
Hence, if it is possible to execute an activity that is strongly compliant, the agent will prefer it over a weakly or non-compliant activity (since the use of these actions is required to allow $policy\_compliant(f)$ to be true).
Under this condition, the policy actions $ignore\_not\_permitted(a)$ and $ignore\_neg\_permitted(a)$ are only used when it is impossible to achieve the fluent $f$ in a strongly or weakly compliant manner, respectively.

The combination of compliance optimization with our previously discussed options allows for more possible configurations.
For example, adding optimization to the \textit{subordinate} option makes a \textit{cautious} mode.
In this mode, the agent will try to mimic the behavior of the \textit{paranoid} mode (all strongly compliant actions), but ultimately it will reduce to \textit{subordinate} (all weakly compliant actions) in the worst case.
Likewise, adding optimization to the \textit{utilitarian} mode adds two options: \textit{best effort} and \textit{subordinate when possible}.
\textit{Best effort} prefers strong compliance over weak compliance and weak compliance over non-compliance, but ultimately permits non-compliance when no better alternatives exist.
\textit{Subordinate when possible} prefers weak compliance over non-compliance but does not optimize from weak compliance to strong compliance.

A new feature of this approach to optimization is the ability to optimize within the weakly and non-compliant categories.
Consider two weakly compliant activities 1 and 2.
While they are both weakly compliant as a whole, one may mostly consist of strongly compliant actions.
Suppose activity 1 has more weakly compliant actions than activity 2.
Since, weakly compliant actions do require a concurrent $ignore\_not\_permitted(a)$ action, activity 1 will have more $ignore\_not\_permitted(a)$ actions than activity 2.
Hence, activity 2 will be preferred to activity 1, even though they both fall in the weakly compliant category.
\Citet{gelfond_authorization_2008} do not consider such a feature.

In the case use of a policy action is required, we also optimize to postpone its use to a later timestep.
This comes from the hope that unexpected exogenous events could change future policy compliance such that actions have a higher level of compliance than what they have currently.
In handling such a case, our optimization resembles a notion of least commitment planning regarding lower-compliance actions.

Our discussion thus far has focused on authorization policies induced by an $\mathcal{AOPL}$ policy.
To address obligation policies, we introduce policy fluents $obl\_compliant(do\_action)$ and \\ $obl\_compliant(refrain\_from\_action)$ with policy actions $ignore\_obl(a)$ and $ignore\_obl(neg(a))$, where $a$ is another action.
(For configurability, we consider obligation policies to do actions and to refrain from actions separately).
We extend the definition of $policy\_compliant(f)$ to require both $obl\_compliant(do\_action)$ and $obl\_compliant(refrain\_from\_action)$ to be true.
Like with authorization compliance, if $obl(a)$ is true but action $a$ does not occur, then $obl\_compliant(do\_action)$ becomes false and remains false by inertia.
Likewise for $obl\_compliant(refrain\_from\_action)$.
If $ignore\_obl(a)$ or $ignore\_obl(neg(a))$ are performed, then these effects on the $obl\_compliant$ fluents are temporarily waived.

There are five different configurations an agent in the $\mathcal{APIA}$ architecture can have regarding its obligation policy.
When in \textit{subordinate} mode, the agent will never use either $ignore\_obl(a)$ and $ignore\_obl(neg(a))$ actions.
Hence, all activities that achieve $policy\_compliant(f)$ will be compliant with both aspects of its obligation policy.
When in \textit{best effort} mode, the agent prefers using other actions over these policy actions.
Hence, activities will be compliant if possible but may include non-compliant elements when no other goal-achieving activities exist.
The \textit{permit omissions where necessary} and \textit{permit commissions where necessary} options are variations of these modes.
\textit{permit omissions where necessary} is like \textit{best effort} with regards to $obl(a)$ policy statements, but like \textit{subordinate} with regards to $obl(\neg a)$ policy statements.
Likewise, \textit{permit commissions where necessary} is like \textit{subordinate} with regards to $obl(a)$ policy statements but like \textit{best effort} regarding $obl(\neg a)$ statements.
\textit{Utilitarian} mode, like with authorization policies, reduces the behavior of an $\mathcal{APIA}$ agent with respect to its obligation policy to that of an $\mathcal{AIA}$ agent.

An agent's combined authorization and obligation policy configuration can be represented by a 2-tuple $(A, O)$, where $A$ is the authorization mode and $O$ is the obligation mode.
For reference, the $\mathcal{APIA}$ modes are summarized in \cref{table:apia_authorization_modes,table:apia_obligation_modes}.
When an $\mathcal{APIA}$ agent is running in mode $(utilitarian, utilitarian)$, its behavior in terms of both physical agent actions and mental agent actions reduce to that of an $\mathcal{AIA}$ agent (i.e.~policy actions are not used in this mode).
This is due to an optimization we provide internally.
For each $\mathcal{APIA}$ configuration, we adjust the definition of $policy\_compliant(f)$ such that excess policy actions are not required.
Since a utilitarian agent completely disgards its policy, we adjust $policy\_compliant(f)$ that it is equivalent to $f$.
In the case of a subordinate agent, we adjust $policy\_compliant(f)$ such that $ignore\_not\_permitted(a)$ is never needed since such an agent always disregards strong compliance.

\begin{table}[h]
    \centering
    \begin{tabular}{ | l | c | c | c | }
        \hline
        & Require weak & Prefer weak over non-compl. & Ok with non-compl. \\
        \hline
        Require strong & Paranoid & \sout{(Invalid)} & \sout{(Invalid)} \\
        \hline
        Prefer strong over weak & Cautious & Best effort & \sout{(Invalid)} \\
        \hline
        Ok with weak & Subordinate & Subordinate when possible & Utilitarian \\
        \hline
    \end{tabular}
    \caption{$\mathcal{APIA}$ authorization policy modes}
    \label{table:apia_authorization_modes}
\end{table}

\begin{table}[h]
    \centering
    \begin{tabularx}{\textwidth}{ | X | X | X | X | }
        \hline
        & Honor $obl(\neg a)$ & Prefer honoring $obl(\neg a)$ if possible & Ignore $obl(\neg a)$ \\
        \hline
        Honor $obl(a)$ & Subordinate & Permit commissions where necessary & \sout{(Not reasonable)} \\
        \hline
        Prefer honoring $obl(a)$ if possible & Permit omissions where necessary & Best effort & \sout{(Not reasonable)} \\
        \hline
        Ignore $obl(a)$ & \sout{(Not reasonable)} & \sout{(Not reasonable)} & Utilitarian \\
        \hline
    \end{tabularx}
    \caption{$\mathcal{APIA}$ obligation policy modes}
    \label{table:apia_obligation_modes}
\end{table}

\section{Examples}

To demonstrate the operations of an agent in the $\mathcal{APIA}$ architecture, we will introduce a series of examples.
For conciseness, we will focus on three $\mathcal{APIA}$ configurations: $(paranoid, subordinate)$, $(best \ effort, best \ effort)$, and $(utilitarian, utilitarian)$.

\subsection{Example A: Fortunate case}

To begin with a simple case, suppose that two agents exist in an office space.
In this office space, there are four rooms with doors in between them.
Room 1 is connected by door $d_{12}$ to Room 2.
Room 2 is connected by door $d_{23}$ to Room 3 and so on.
Door $d_{34}$ has a lock and is currently in the unlocked position.
Suppose our agent, Alice, wants to greet another agent, Bob.

This scenario can be represented by \cref{fig:apia_example_a_visual,fig:apia_example_a_description}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{Figures/AIA_Architecture/Example_1}
    \caption{Visual depiction of Example A}
    \label{fig:apia_example_a_visual}
\end{figure}

\begin{figure}[h]
    \begin{framed}
        Fluents:
        \begin{itemize}
            \item $door\_locked(D)$, for each door $D$
            \item $in\_room(P, R)$
            \item $greeted\_by(P, A)$, where person $P$ is greeted by person $A$
        \end{itemize}
        where $R$ is a room, $D$ is a door, $P$ is a person.

        Actions:
        \begin{itemize}
            \item $move\_through(A, D)$
            \item $lock\_door(A, D)$
            \item $unlock\_door(A, D)$
            \item $greet(A, P)$
        \end{itemize}
        where $A$ is the person doing the action, $D$ is a door, and $P$ a person (the direct object of the action).

        Alice is given the following policy:
        \begin{gather}
            \label{eq:apia_example_a_permitted_move_through}
            permitted(move\_through(A, D)) \\
            permitted(lock\_door(A, D)) \\
            permitted(unlock\_door(A, D)) \\
            permitted(greet(A, P)) \\
        \end{gather}

        The following activities are in Alice's library.
        \begin{equation}
        \begin{split}
            <1, [
                & move\_through(alice, d_{12}), \\
                & move\_through(alice, d_{23}), \\
                & move\_through(alice, d_{34}), \\
                & greet(alice, bob) \\
            ], & \ policy\_compliant(greeted\_by(alice, bob))> \\
        \end{split}
        \end{equation}
    \end{framed}
    \caption{Description of Example A}
    \label{fig:apia_example_a_description}
\end{figure}

Before the $\mathcal{AIA}$ control loop begins, Alice observes that she is in Room 1, Bob is in room 4, the door $d_{34}$ is unlocked, and that she has not yet greeted Bob.

At timestep 0, the first iteration of the $\mathcal{AIA}$ control loop begins.
In this first step, Alice analyzes her observations and interprets unexpected observations by assuming undetected exogenous actions occurred.
None of her observations are unexpected, so no exogenous actions are assumed to occur.
Alice then intends to wait at timestep 0.
Alice attempts wait.
Alice observes that her wait action was successful and that, in the meantime, the $select(policy\_compliant(greeted\_by(alice, bob)))$ action happened.
The timestep is incremented and Alice does not observe any fluents.

The second iteration of the $\mathcal{AIA}$ control loop begins.
Alice analyzes her observation of \\ $select(policy\_compliant(greeted\_by(alice, bob)))$ and determines that \\ $active\_goal(policy\_compliant(greeted\_by(alice, bob)))$ is true.
Alice then starts planning to achieve $policy\_compliant(greeted\_by(alice, bob))$ and determines that she intends to start activity 1.
Since each action in activity 1 is strongly compliant, no policy actions are needed.

The rest of the example is very straight forward and is almost identical to scenarios discussed by \citet{blount_architecture_2013,blount_towards_2014} in the $\mathcal{AIA}$ architecture.
However, let us transition from this fortunate example to one where a strongly compliant activity becomes weakly compliant due to an unexpected environmental observation.

\subsection{Example B: Strong compliance degrades to weak compliance}

In the same scenario, suppose we modify Alice's policy such that regarding $greet(A, P)$ we have:
\begin{gather}
    \label{eq:apia_example_b_permitted_greet}
    permitted(greet(A, P)) \textbf{ if }
        \neg busy\_working(P) \\
    \begin{split}
        permitted(greet(A, P)) \textbf{ if }
            & busy\_working(P), \\
            & in\_room(P, R), \\
            & door\_connects(D, R), \\
            & knocked\_on\_door(D)
    \end{split}
\end{gather}
along with other additions listed in \cref{fig:apia_example_b_description}.

Before the $\mathcal{AIA}$ control loop begins, Alice also observes that Bob is not busy working (that is, in addition to her initial observations of Example A).
At timestep 0, the first iteration of the control loop begins.
During the second iteration of the control loop (at timestep 1), Alice plans to achieve the $policy\_compliant(greeted\_by(alice, bob))$ goal.
Since she believes Bob is not busy working, activity 1 is still strongly compliant.

She then executes activity 1 like in Example A until she enters Room 3, at which points she observes that Bob is busy working.
During the next iteration (at timestep 5), the agent interprets this observation by inferring that $begin\_working(bob)$ happened at the previous timestep (4).

Here activity 1 is weakly compliant.
Since Bob is busy working but Alice has not knocked on the door, no policy statement describes whether our next action ($greet(alice, bob)$) is compliant or not.
If Alice is operating in $(utilitarian, utilitarian)$ mode, she continues the execution of activity 1 and greets Bob anyway.
(This happens without the use of policy actions due to our internal optimizations).
Otherwise, our agent will stop the activity and then either refuse to plan another weakly compliant activity or use a concurrent policy action to dismiss this event.

If our agent is running in $(paranoid, subordinate)$, Alice will refuse to execute a weakly compliant activity.
Through planning, it will discover that a new activity that includes knocking at the door is strongly compliant (e.g.~activity 3) and begin its execution.
If our agent is running in $(best \ effort, best \ effort)$, it will behave likewise because activity 3 is strongly compliant.
The difference is that, if there did not exist a strongly compliant activity, it would plan a new activity that involved a policy action and greeted Bob anyway.

Alice knocks on door $d_{34}$ at timestep 7, greets Bob at timestep 8, stops activity 3 at timestep 9, and proceeds to wait for all future timesteps.

\subsection{Example C: Compliance degrades to non-compliant}

Suppose we take \cref{eq:apia_example_b_permitted_greet}, make it defeasible, and add this $\mathcal{AOPL}$ rule in addition (See also \cref{fig:apia_example_c_description}):
\begin{equation}
\begin{split}
    \neg permitted(greet(A, P)) \textbf{ if }
        & busy\_working(P), \\
        & supervisor\_to(P, A) \\
\end{split}
\end{equation}

Now, let us imagine that Bob is Alice's supervisor.
Similar to Example B, our agent executes activity 1 until the observation that Bob is busy working.
This time, we have a strict authorization statement forbidding greeting Bob since he is Alice's supervisor.
Under the $(utilitarian, utilitarian)$ option, we proceed on with activity 1 anyway.
With the $(paranoid, subordinate)$ option, our agent stops activity 1 but cannot construct a new activity that achieves the goal subject to its policy.
Hence, the goal is futile and the agent waits until its environment changes such that a strongly compliant activity exists.
Under the $(best \ effort, best \ effort)$ option however, our agent constructs a new activity that contains greets Bob anyway.
The activity contains: $greet(alice, bob)$, $ignore\_not\_permitted(greet(alice, bob))$, and $ignore\_neg\_permitted(greet(alice, bob))$.

\subsection{Example D: Hierarchy of contradictory defeasible statements}

Further extending Example C, suppose we make \cref{eq:apia_example_a_permitted_move_through} defeasible:
\begin{equation}
\begin{split}
    \label{eq:apia_example_d_move_normal}
    move\_normal(A, D): \\
    \textbf{ normally } permitted(move\_through(A, D))
\end{split}
\end{equation}
and add the following policy statement:
\begin{equation}
\begin{split}
    \label{eq:apia_example_d_move_office}
    move\_office(A, D): \\
    \textbf{ normally } \neg permitted(move\_through(A, D)) \textbf{ if }
        & in\_room(A, R_1), \\
        & door\_connects(D, R_2), \\
        & R_1 != R_2
\end{split}
\end{equation}
and the following static (See also \cref{fig:apia_example_d_description}):
\begin{equation}
\begin{split}
    private\_office(r_4).
\end{split}
\end{equation}

Since we have two contradictory defeasible statements, we need to add a preference between the two (without a preference our agent can non-deterministically choose between which of the two rules to apply).
Suppose we add:
\begin{equation}
\begin{split}
    prefer(move\_office(A, D), move\_normal(A, D))
\end{split}
\end{equation}

Then, when Alice observes that Bob is not busy working at the beginning of the scenario, an agent running in $(paranoid, subordinate)$ mode will immediately consider the goal to be futile.
Unlike in Example C, our agent knows this immediately because $private\_office$ is a static, not an unexpected observation.
If our agent is running in $(best \ effort, best \ effort)$ mode, it creates an activity like activity 1, except that it contains $ignore\_not\_permitted(greet(alice, bob))$ and $ignore\_neg\_permitted(greet(alice, bob))$.
Our utilitarian agent, like always, completely ignores our policy and executes activity 1.

Should we have written:
\begin{equation}
\begin{split}
    prefer(move\_normal(A, D), move\_office(A, D))
\end{split}
\end{equation}
instead, then the second \cref{eq:apia_example_d_move_office} would have no effect since \cref{eq:apia_example_d_move_normal} does not have an ``if'' condition.
The rest of the example would model Example C.

\subsection{Example L: Fortunate case}

In the following examples, we shift our focus from authorization policies to obligation policies.
In the same office space, we suppose we have statics, fluents, and actions as described in \cref{fig:apia_example_l_description}.
Suppose it is drawing close to the end of the day and we want to close and secure the workspace for the night.
Specifically, we want to enable the premises alarm system.
This alarm system can be enabled from anywhere within the building.
For simplicity, we do not consider the logistics of ensuring our agents or other personnel are next to building exits, lest they trigger the alarm that they turned on.

At the start of this scenario, Alice observes she is in room $r_{1}$, Bob is in room $r_{4}$, and the security system is off.
At timestep 0, we observes the exogenous mental action \\ $select(policy\_compliant(security\_system\_enabled))$.
Note, since the alarm system can be enabled from any location, a $(utilitarian, utilitarian)$ agent starts activity 2.
However, our \\ $(paranoid, subordinate)$ and our $(best \ effort, best \ effort)$ agents foresee activity 1 to be policy compliant.
Hence, an agent in either mode starts activity 1 (they will walk through the entire building and turn off all lights and lock door $d_{34}$).

\subsection{Example M: All activities become non-compliant}

If we were to add to Example L a common curtesy to refrain from turn off lights on other people:
\begin{equation}
\begin{split}
    \label{apia:example_m_obl_neg_turn_lights_off}
    obl(\neg turn\_lights\_off(A, R)) \textbf{ if }
        & person(P) \\
        & \textbf{not } agent(P) \\
        & in\_room(P, R) \\
\end{split}
\end{equation}

Then, our agent behavior is as follows.
Our $(utilitarian, utilitarian)$ agent will start activity 2 as before.
In other modes, since Alice has initially observed that Bob is in room $r_{4}$, she cannot construct a policy-compliant activity that achieves $security\_system\_enabled$.
As a result, a $(paranoid, subordinate)$ agent will determine that the goal is futile and proceed to wait perpetually until she observes that Bob leaves the building.
A $(best \ effort, best \ effort)$ agent executes activity 3, which contains $ignore\_obl(neg(turn\_lights\_off(A, R)))$, since no compliant alternatives exist.

\subsection{Example N: Active activity becomes non-compliant. Compliant alternatives exist}

To demonstrate a case where a compliant alternative exists, we add to Example M the ability to ask someone to move.
One can think of the $ask\_to\_move\_through$ action as a friendly escort action that allows our agent to move another person.
In order to create a place for Bob to be moved to so that the security system can be enabled, let us create a ``room'' that represents the outdoors with a door $d_{3exit}$ connecting room $r_{3}$ to the outside.

For the sake of the example, suppose we initially observe Bob to be outside.
Then, given the observation of $select(policy\_compliant(security\_system\_enabled))$, we start activity 1.
When Alice moves into room $r_{4}$, we observe that Bob is now in room $r_{4}$ with her.
This causes Alice to diagnose two exogenous actions: $move\_through(bob, d_{3exit})$ and $move\_through(bob, d_{34})$.
Assuming our agent is in either $(paranoid, subordinate)$ or $(best \ effort, best \ effort)$, Alice will stop the active activity and plan to move Bob so that she can achieve $security\_system\_enabled$ without violating her policy.
Alice starts activity 4.

Notice that Alice does not bring Bob outside before turning on the security system.
This is because our policy only forbids turning off lights while others are present.
It does not forbade bringing others into an already-dark room.
If we wanted this feature, we could add the following policy statement and our agent Alice would plan to turn on the lights ahead of time, so that she can bring Bob outside, then go back in to turn off all of the lights.
\begin{equation}
\begin{split}
    obl(\neg ask\_to\_move\_through(A, D)) \textbf{ if }
        lights\_off(ToR)
\end{split}
\end{equation}

\subsection{Example O: Hierarchy of contradictory defeasible statements}

% TODO: Add reference to YY
If we make \cref{apia:example_m_obl_neg_turn_lights_off} defeasible, we can consider how the scenario would change under different prefer hierarchies.
\begin{gather}
\begin{split}
    lights\_someone\_else(A, R): \\
    \textbf{ normally } obl(\neg turn\_lights\_off(A, R)) \textbf{ if } \\
        & person(P) \\
        & \textbf{not } agent(P) \\
        & in\_room(P, R) \\
\end{split} \\
\begin{split}
    lights\_security\_system(A): \\
    \textbf{ normally } obl(\neg enable\_security\_system(A)) \textbf{ if }
        \neg all\_lights\_off
\end{split}
\end{gather}

If we have:
\begin{equation}
\begin{split}
    prefer(lights\_someone\_else(A, R), lights\_security\_system(A))
\end{split}
\end{equation}

Then the $lights\_security\_system$ rule is waived when the condition for the $lights\_someone\_else$ rule is true.
As a result, when Alice sees Bob is in room $r_{4}$, she only chooses to honor one rule and will not turn off the lights on Bob.
She will stop her activity and start activity 2.

On the other hand, if we had:
\begin{equation}
\begin{split}
    prefer(lights\_someone\_else(A, R), lights\_security\_system(A))
\end{split}
\end{equation}

Then the rule against turning the lights off on Bob is ignored.
As a result, Alice will continue her activity to turn the light in room $r_{4}$ and then enable the security system.

% \subsection{Example S}
%
% \subsection{Example T}
