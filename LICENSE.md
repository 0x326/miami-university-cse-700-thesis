# License

## LaTeX document, figures, & misc. files (excluding code-snippets)

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">An Architecture for Policy-Aware Intentional Agents</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/0x326/miami-university-cse-700-thesis" property="cc:attributionName" rel="cc:attributionURL">John Meyer</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.<br />Permissions beyond the scope of this license may be available at <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/0x326/miami-university-cse-700-thesis/-/blob/2021-04-17-thesis/LICENSE.md" rel="cc:morePermissions">https://gitlab.com/0x326/miami-university-cse-700-thesis/-/blob/2021-04-17-thesis/LICENSE.md</a>.

## Original code

> E.g. `Figures/APIA_*`

```
MIT License

Copyright (c) 2021 John Meyer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

## Reproduced code under fair-use

Insofar as I am able, I release code files and snippets which I copied from other publications under the same MIT license as above.
