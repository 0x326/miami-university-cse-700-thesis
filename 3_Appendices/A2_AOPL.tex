\chapter{Language $\mathcal{AOPL}$}
\label{appendix:aopl}

\section{Checking Compliance}
\label{appendix:aopl_compliance}

To evaluate the compliance of happening $h$ at state $s$ with an obligation policy $P$, \citet{gelfond_authorization_2008} extend $lp$ as follows:

\begin{gather}
    lp(obl(h)) =_{def}
        obl(h). \\
    lp(\neg obl(h)) =_{def}
        \neg obl(h). \\
    lp(obl(h) \textbf{ if } F) =_{def}
        obl(h) \leftarrow
            lp(F). \\
    lp(\neg obl(h) \textbf{ if } F) =_{def}
        \neg obl(h) \leftarrow
            lp(F). \\
    lp(d_i: \textbf{normally } obl(h) \textbf{ if } F_i) =_{def}
        obl(h) \leftarrow
            lp(F),
            \textbf{not } ab(d),
            \textbf{not } \neg obl(h). \\
    lp(d_j: \textbf{normally } \neg obl(h) \textbf{ if } F_j) =_{def}
        \neg obl(h) \leftarrow
            lp(F),
            \textbf{not } ab(d),
            \textbf{not } obl(h). \\
    lp(prefer(d_i, d_j)) =_{def}
        ab(d_j) \leftarrow lp(F_1).
\end{gather}

\begin{definition}
    $obl(h) \in P(s)$ iff the logic program $lp(P, s)$ entails $obl(h)$
\end{definition}

\begin{definition}
    \label{def:obligation_event_compliance}
    An event $<s, A>$ is \textit{compliant} with an obligation policy $P$ if $(\forall obl(a) \in P(s) \ a \in A) \land (\forall obl(\neg a) \in P(s) \ a \not \in A)$
\end{definition}

This definition of compliance contrasts with that for authorization policies.
Given a set of actions $A$, the definition for authorization policies can be thought of first enumerating action $a \in A$ then checking to see if $permitted(a)$ exists.
The definition for obligation policies, by contrast, can be thought of first searching for $obl(a)$ statements, then checking to see if $a \in A$ exists (or $a \not \in A$ in the case of $obl(\neg a)$).

Furthermore, the definition for obligation compliance does not differentiate between strong compliance and weak compliance.
Were such a differentiation to exist, it would center around instances where neither $\neg obl(a)$ nor $obl(a)$ is true\footnotemark.
For such a case, an obligation policy would not explicitly impose nor explicitly waive an agent from an obligation to perform action $a$.
However, since $\mathcal{AOPL}$ does not consider `weak compliance', this situation is simply called \textit{compliant}.

\footnotetext{
    A similar statement can be made about when neither $\neg obl(\neg a)$ nor $obl(\neg a)$ is true.
}

\begin{definition}
    Let $P$ be an arbitrary policy $P$ written in $\mathcal{AOPL}$.

    \begin{itemize}
        \item Let $P_a$ be a derivative policy of $P$ that only has the authorization statements of $P$.
            $P_a$ is an \textit{authorization policy induced by} $P$~\citep{gelfond_authorization_2008}.
        \item Likewise, let $P_o$ be a derivative of $P$ such that $P_o$ only has the obligation statements of $P$.
            $P_o$ is an \textit{obligation policy induced by} $P$~\citep{gelfond_authorization_2008}.
    \end{itemize}
\end{definition}

\begin{definition}
    Let $P$ be an arbitrary policy $P$ written in $\mathcal{AOPL}$.
    Let $P_a$ and $P_o$ be the authorization and obligation policy induced by $P$, respectively.

    \begin{itemize}
        \item An event $<s, A>$ is \textit{strongly compliant} with $P$ if $<s, A>$ is strongly compliant with $P_a$ and if $<s, A>$ is compliant with $P_o$~\citep{gelfond_authorization_2008}.
        \item An event $<s, A>$ is \textit{weakly compliant} with $P$ if $<s, A>$ is weakly compliant with $P_a$ and if $<s, A>$ is compliant with $P_o$~\citep{gelfond_authorization_2008}.
        \item An event $<s, A>$ is \textit{non-compliant} with $P$ if $<s, A>$ is neither strongly compliant nor weakly compliant with $P$\footnotemark.
    \end{itemize}
\end{definition}

\footnotetext{
    This definition is not explicitly provided by \citet{gelfond_authorization_2008}.
}

\section{Obligation Example}
\label{appendix:aopl_example}

The policy $P$ consisting of \cref{eq:aopl_example_line_1,eq:aopl_example_line_2,eq:aopl_example_line_3,eq:aopl_example_line_4,eq:aopl_example_line_6,eq:aopl_example_line_7} can be encoded as follows\footnotemark:
\lstinputlisting[language=Prolog]{Figures/AOPL_Language/01_Full_Knowledge/P.lp}

\footnotetext{
    We have added all of the following ASP code to supplement the discussion by \citet{gelfond_authorization_2008}.
}

To keep logic programs in this example from becoming unnecessarily long, the above source (as well as those that will follow it) will favor the use of ASP variables over the manual enumeration of primitive variable values.
To bind variable values to an ASP variable, we will use the following convention:
\begin{itemize}
    \item If the variable $Student$ ranges over values $student_1, student_2, \dots, student_n$, then it will be encoded as follows:
        \begin{gather}
            student(student_1). \\
            student(student_2). \\
            \dots \\
            student(student_n).
        \end{gather}
    \item If the variable $Class$ ranges over values $class_1, class_2, \dots, class_n$, then it will be encoded as follows:
        \begin{gather}
            class(class_1). \\
            class(class_2). \\
            \dots \\
            class(class_n).
        \end{gather}
    \item If the variable $MeetingNum$ ranges over values $1, 2, \dots, n$, then it will be encoded as follows:
        \begin{equation}
            1, 2, \dots, n
        \end{equation}
    \item If the variable $AssignmentNum$ ranges over values $1, 2, \dots, n$, then it will be encoded as follows:
        \begin{equation}
            1, 2, \dots, n
        \end{equation}
\end{itemize}

\Citet{gelfond_authorization_2008} do not provide a concrete example that instantiates all of the above variables.
However, in order to provide a complete discussion of their work, we will use the following variables and the following state $s_0$:
\lstinputlisting[language=Prolog]{Figures/AOPL_Language/01_Full_Knowledge/instantiate_variables.lp}
\lstinputlisting[language=Prolog]{Figures/AOPL_Language/01_Full_Knowledge/s0.lp}

Suppose we want to check the compliance of the following occurance of actions $A$:
\lstinputlisting[language=Prolog]{Figures/AOPL_Language/01_Full_Knowledge/A.lp}

We can check for compliance using the following logic programs\footnotemark:
\footnotetext{
    In what follows, we implement a generic logic program that is capable of checking the compliance of any set of actions $A$ with respect to any arbitrary policy $P$.
    This is for added convenience, since this logic program can simultaneously check the compliance of $A$ with the induced authorization policy $P_a$ as well as the induced obligation policy $P_o$.
    This is a significant improvement from the original definitions presented in \cref{def:authorization_event_compliance_full_knowledge,def:obligation_event_compliance}, since the latter involves invoking \textsc{clingo} twice (once for checking compliance of $A$ with $P_a$ and again for $P_o$) and hardcodes $A$ into the logic program.
}

\lstinputlisting[language=Python]{Figures/AOPL_Language/01_Full_Knowledge/Compliance_Checks/util.lp}
\lstinputlisting[language=Prolog]{Figures/AOPL_Language/01_Full_Knowledge/Compliance_Checks/authorization_compliance.lp}
\lstinputlisting[language=Prolog]{Figures/AOPL_Language/01_Full_Knowledge/Compliance_Checks/obligation_compliance.lp}

In the above snippet, the inline Python script is instrumental in differentiating between $obl(a)$ and $obl(\neg a)$.
Given the ASP expression $obl(Action)$, the variable $Action$ can be bound to either $a$ or $\neg a$ (in the case $obl(a)$ is true or $obl(\neg a)$ is true, respectively).
Since $obl(a)$ and $obl(\neg a)$ have different semantics in $\mathcal{AOPL}$, they need to be addressed by two separate rules.
By writing $obl(Action), \ @is\_positive(Action) = true$, the variable $Action$ can only be bound to $a$ (hence only $obl(a)$ will be matched).
To address $obl(\neg a)$, we write $obl(-Action), \ @is\_positive(Action) = true$.

Finally, if we add:
\lstinputlisting[language=Prolog]{Figures/AOPL_Language/01_Full_Knowledge/Compliance_Checks/strongly_compliant.lp}
we can see that the event $<s_0, A>$ is not strongly compliant:
\lstinputlisting{Figures/AOPL_Language/01_Full_Knowledge/Scripts/01/strongly_compliant.snippet.txt}

Upon further investigation (which is not shown here for brevity), one can see that this is because our policy $P$ does not have any authorization statements.
Hence, no action is strongly compliant.
However, the event $<s_0, A>$ is weakly compliant:
\lstinputlisting[language=Prolog]{Figures/AOPL_Language/01_Full_Knowledge/Compliance_Checks/weakly_compliant.lp}
\lstinputlisting{Figures/AOPL_Language/01_Full_Knowledge/Scripts/01/weakly_compliant.snippet.txt}

For completeness, the event $<s_0, A>$ is not non-compliant:
\lstinputlisting[language=Prolog]{Figures/AOPL_Language/01_Full_Knowledge/Compliance_Checks/non_compliant.lp}
\lstinputlisting{Figures/AOPL_Language/01_Full_Knowledge/Scripts/01/non_compliant.snippet.txt}
