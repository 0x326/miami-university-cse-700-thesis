\chapter{Language $\mathcal{APL}$}
\label{appendix:apl}

\section{Checking Compliance}
\label{appendix:apl_compliance}

As mentioned previously, \citet{gelfond_authorization_2008} use ASP to check the compliance of an action $A$ at state $s$ with a policy $P$.
To do so, \citet{gelfond_authorization_2008} define the function $lp(P,s)$ to translate $P$ into an executable ASP program as follows:

\begin{gather}
    lp(P,s) =_{def} lp(P)\cup lp(s). \label{eq:apl_lp_01} \\
    lp(P)=_{def}\{lp(statement) | statement \in P\} \label{eq:apl_lp_02} \\
    lp\left(V(f,s)=y\right) =_{def}
        val\left(f,y\right). \label{eq:apl_lp_03} \\
    lp(permitted(a)) =_{def}
        permitted(a). \label{eq:apl_lp_04} \\
    lp(\neg permitted(a)) =_{def}
        \neg permitted(a). \label{eq:apl_lp_05} \\
    lp(permitted(a) \textbf{ if } F) =_{def}
        permitted(a) \leftarrow
            lp(F). \label{eq:apl_lp_06} \\
    lp(\neg permitted(a) \textbf{ if } F) =_{def}
        \neg permitted(a) \leftarrow
            lp(F). \label{eq:apl_lp_07}
\end{gather}
\begin{multline}
    \label{eq:apl_lp_08}
    lp(d_i: \textbf{normally } permitted(a) \textbf{ if } F_i) =_{def} \\
        permitted(a) \leftarrow
            lp(F),
            \textbf{not } ab(d),
            \textbf{not } \neg permitted(a).
\end{multline}
\begin{multline}
    \label{eq:apl_lp_09}
    lp(d_j: \textbf{normally } \neg permitted(a) \textbf{ if } F_j) =_{def} \\
        \neg permitted(a) \leftarrow
        lp(F),
        \textbf{not } ab(d),
        \textbf{not } permitted(a).
\end{multline}
\begin{gather}
    lp(prefer(d_i, d_j)) =_{def}
        ab(d_j) \leftarrow lp(F_1). \label{eq:apl_lp_10}
\end{gather}
where $lp(s)$ is the logic encoding of the current state, following an approach like that by \citet{balduccini_aaa_2008}.
If S is a set of atoms, then $lp(S)=_{def}\{lp(atom) | atom \in S\}$

In order to maintain a continuity of syntax between the work of \citet{gelfond_action_1998,balduccini_aaa_2008,blount_architecture_2013,gelfond_authorization_2008}, if $\boldsymbol{V} = \{t,f\}$, this paper will adjust $lp$ such that the following translation written in syntax provided by \citet{gelfond_authorization_2008}:
\begin{equation}
    lp\left(V(f,s)=y\right) =_{def}
        val\left(f,y\right).
\end{equation}
will be simplified to resemble the syntax of \citet{gelfond_action_1998,balduccini_aaa_2008,blount_architecture_2013} as follows:
\begin{gather}
    lp\left(V(f,s)=t\right) =_{def}
        f. \\
    lp\left(V(f,s)=f\right) =_{def}
        \neg f. \\
\end{gather}

\begin{definition}
    \label{def:lp_consistent}
    \label{def:lp_categorical}
    ~

    \begin{itemize}
        \item A logic program is \textit{consistent} if it produces at least one answer set~\citep{gelfond_authorization_2008}.
        \item A logic program is \textit{categorical} if it produces exactly one answer set~\citep{gelfond_authorization_2008}.
    \end{itemize}
\end{definition}

\begin{definition}
    \label{def:authorization_consistent}
    \label{def:authorization_categorical}
    ~

    \begin{itemize}
        \item An authorization policy $P$ for transition system $T$ is \textit{consistent} if for every state $s$ of $T$, logic program $lp(P, s)$ is consistent~\citep{gelfond_authorization_2008}.
        \item An authorization policy $P$ for transition system $T$ is \textit{categorical} if for every state $s$ of $T$, logic program $lp(P, s)$ is categorical~\citep{gelfond_authorization_2008}.
    \end{itemize}
\end{definition}

\begin{definition}
    \label{def:entails}
    If a consistent logic program $\Pi$ has a literal $l$ which belongs to every answer set of $\Pi$, $\Pi$ \textit{entails} $l$~\citep{gelfond_authorization_2008}.
\end{definition}

\begin{definition}
    Let $P$ be a consistent authorization policy for transition system $T$.

    \begin{itemize}
        \item $permitted(a) \in P(s)$ iff a logic program $lp(P, s)$ entails $permitted(a)$~\citep{gelfond_authorization_2008}.
        \item $\neg permitted(a) \in P(s)$ iff a logic program $lp(P, s)$ entails $\neg permitted(a)$~\citep{gelfond_authorization_2008}.
    \end{itemize}
\end{definition}

\begin{definition}
    \label{def:authorization_event_compliance}
    ~

    \begin{itemize}
        \item An event $<s, A>$ is \textit{strongly compliant} with $P$ if~\citep{gelfond_authorization_2008}:
            \begin{equation}
                \forall a \in A\ permitted(a) \in P(s)
            \end{equation}
        \item An event $<s, A>$ is \textit{weakly compliant} with $P$ if~\citep{gelfond_authorization_2008}:
            \begin{equation}
                \forall a \in A\ \neg permitted(a) \not \in P(s)
            \end{equation}
        \item An event $<s, A>$ is \textit{non-compliant} with $P$ if~\citep{gelfond_authorization_2008}:
            \begin{equation}
                \exists a \in A\ \neg permitted(a) \in P(s)
            \end{equation}
    \end{itemize}
\end{definition}

\begin{definition}
    \label{def:authorization_path_compliance}
    A path $<s_0, A_0, s_1, A_1, \dots, s_{n-1}, A_{n-1}, s>$ of transition system $T$ can be similarly categorized into \textit{strongly compliant} or \textit{weakly compliant} if every event $<s_i, A_i>$ along the path for $0 \le i < n$ also has the same classification~\citep{gelfond_authorization_2008}.
    If the path has at least one non-compliant event, then the path is \textit{non-compliant}.
\end{definition}

To demonstrate compliance checking, \citet{gelfond_authorization_2008} will use their working example in the context of particular missions and commanders.
Let $P_m$ be a policy constructed in the form of $P$ for a mission $m_1$ and a colonel $c_1$.
$P_m$ is as follows:
\begin{gather}
    d_1(c_1, m_1): \textbf{normally } \neg permitted(assume\_command(c_1, m_1)) \textbf{ if } authorized(c_1, m_1) \\
    d_2(c_1, m_1): \textbf{normally } permitted(assume\_command(c_1, m_1)) \textbf{ if } colonel(c_1) \\
    prefer(d_2(c_1,m_1),d_1(c_1,m_1)) \\
    \neg permitted(authorize(c_1, m_1)) \textbf{ if } observer(c_1)
\end{gather}

The logic translation of $P_m$, $lp(P_m)$ is the following:
\lstinputlisting[language=Prolog]{Figures/APL_Language/01_Initial_Example/Pm.lp}

Suppose the agent's environment can be represented by the following state:
\begin{equation}
    s_0 = \{colonel(c1), authorized(c1,m1), \neg commands(c1,m1), \neg observer(c1) \}
\end{equation}
Its logic encoding $lp(s_0)$ would be:
\lstinputlisting[language=Prolog]{Figures/APL_Language/01_Initial_Example/s0.lp}

Then, to check the compliance of actions $A \in \mathcal{P}(\boldsymbol{A})$, one would first execute the logic program in an ASP grounder and solver such as \textsc{clingo}.
Doing so, we can see that the logic program $lp(P_m, s_0)$ entails the following answer set:
\lstinputlisting{Figures/APL_Language/01_Initial_Example/run.snippet.txt}

Considering actions $A=\{assume\_command(c_1, m_1)\}$, since $a = assume\_command(c_1, m_1)$ is the only element of $A$ and $lp(P_m, s_0)$ entails $permitted(assume\_command(c_1, m_1))$, $A$ is strongly compliant with $P_m$.

For completeness, one might be curious to consider an action such as:
\begin{equation}
    A = \{assume\_command(c_1, m_1), authorize(c_1, m_1)\}
\end{equation}
While such an action would be weakly compliant according to $P_m$ (since $permitted(authorize(c_1, m_1))$ is missing but neither $\neg permitted(assume\_command(c_1, m_1))$ or $\neg permitted(authorize(c_1, m_1))$ is present), a designer would likely render this combination of actions invalid by accompanying $P_m$ with an action description in $\mathcal{AL}$.
Such an action description might look like the following:

\begin{gather}
    \neg observer(c_1) \textbf{ if } colonel(c_1) \label{eq:apl_action_description_5} \\
    authorize(c_1, m_1) \textbf{ causes } authorized(c_1, m_1) \label{eq:apl_action_description_1} \\
    assume\_command(c_1, m_1) \textbf{ causes } commands(c_1, m_1) \label{eq:apl_action_description_2} \\
    \textbf{impossible } authorize(c_1, m_1) \textbf{ if } authorized(c_1, m_1) \label{eq:apl_action_description_3} \\
    \textbf{impossible } assume\_command(c_1, m_1) \textbf{ if } commands(c_1, m_1) \label{eq:apl_action_description_4}
\end{gather}

Intuitively, if $c_1$ has already authorized $m_1$, he cannot authorize it again.
Likewise, if $c_1$ has already assumed command of $m_1$, he cannot begin to assume command of it again.
Thus, actions $A = \{assume\_command(c_1, m_1), authorize(c_1, m_1)\}$ is impossible and is not a valid candidate for checking policy compliance.
$A = \{authorize(c_1, m_1)\}$ is also impossible.

Considering $A=\emptyset$, $A$ is possible according to the action description.
With respect to $P_m$, $A$ is weakly compliant since neither $permitted(wait)$ or $\neg permitted(wait)$ is entailed by $lp(P_m, s_0)$.

\subsection{Checking Compliance with Complete Knowledge of State}

To formalize this approach even further, \citet{gelfond_authorization_2008} provide propositions that decide how compliant an event $<s,a>$ is with an authorization policy $P$.

\begin{definition}
    \label{def:authorization_event_compliance_full_knowledge}
    ~

    \begin{itemize}
        \item An event $<s, a>$ is \textit{strongly compliant} with a consistent policy $P$ for transition system $T$ iff the following logic program is inconsistent~\citep{gelfond_authorization_2008}:
            \begin{equation}
                lp(P, s) \cup \{ \leftarrow permitted(a). \}
            \end{equation}
        \item An event $<s, a>$ is \textit{weakly compliant} with a consistent policy $P$ for transition system $T$ iff the following logic program is inconsistent\footnotemark~\citep{gelfond_authorization_2008}:
            \begin{equation}
                lp(P, s) \cup \{ \leftarrow \textbf{not } \neg permitted(a). \}
            \end{equation}
        \item An event $<s, a>$ is \textit{non-compliant} with a consistent policy $P$ for transition system $T$ iff the following logic program is inconsistent~\citep{gelfond_authorization_2008}:
            \begin{equation}
                lp(P, s) \cup \{ \leftarrow \neg permitted(a). \}
            \end{equation}
    \end{itemize}
\end{definition}

\footnotetext{
    To improve the readability of this document, the definition of weak compliance has been adjusted from how \citet{gelfond_authorization_2008} define it.
    \citet{gelfond_authorization_2008} define weakly compliance to be when the logic program $lp(P, s) \cup \{ \leftarrow \textbf{not } \neg permitted(a).\}$ is consistent.
    The provided definition is a simple logical negation of this definition and, hence, is equivalent to it.
}

These propositions are just simple extensions of \cref{def:authorization_event_compliance}.
However, they are useful because they cause an ASP solver to output a Yes/No answer to the problem.
So, continuing the previous example, one would write:
\begin{lstlisting}[language=Prolog]
:- permitted(assume_command(c1, m1)).
\end{lstlisting}
The logic program $lp(P, s) \cup \{ \leftarrow permitted(assume\_command(c_1, m_1)). \}$ results in:
\lstinputlisting{Figures/APL_Language/02_Full_Knowledge/strongly_compliant.snippet.txt}
Since the output is ``unsatisfiable'', $lp(P, s) \cup \{ \leftarrow permitted(a). \}$ is inconsistent and $<s_0, A>$ is strongly compliant.

\subsection{Checking Compliance with Partial Knowledge of State}

It is worth noting that the above approach requires complete knowledge of the state $s$ (i.e.~the value of all fluents in $s$).
\Citet{gelfond_authorization_2008} show that checking policy compliance is also possible when an agent only knows the value of a subset of fluents in $s$.
Let $\boldsymbol{F_{lim}} \subseteq \boldsymbol{F}$ be the subset of fluents for which the agent knows $V(f, s)$ where $f \in \boldsymbol{F_{lim}}$.
Let $V_{lim} = \{V(f, s) | f \in \boldsymbol{F_{lim}}\}$.

\begin{definition}
    \label{def:simple_knowledge_state}
    In contrast to a proper \textit{state} of an action description (where values are assigned to all fluents, provided they satisfy all static laws), $V_{lim}$ describes a \textit{simple-knowledge state} where values are assigned only to some fluents, provided they satisfy relevant static laws~\citep{gelfond_authorization_2008}.
\end{definition}

\begin{definition}
    Let $\delta(V_{lim}) \in S$ be the set of states that contain $V_{lim}$.

    \begin{itemize}
        \item If $\forall s_{possible} \in \delta(V_{lim})$ event $<s_{possible}, a>$ is strongly compliant with $P$, then $a$ is strongly compliant in $s$~\citep{gelfond_authorization_2008}.
        \item If $\forall s_{possible} \in \delta(V_{lim})$ event $<s_{possible}, a>$ is at least weakly compliant with $P$, then $a$ is at least weakly compliant in $s$~\citep{gelfond_authorization_2008}.
        \item If $\forall s_{possible} \in \delta(V_{lim})$ event $<s_{possible}, a>$ is non-compliant with $P$, then $a$ is non-compliant in $s$~\citep{gelfond_authorization_2008}.
        \item If none of the above hold, then $V_{lim}$ does not hold enough information to determine the compliance of $a$ in $s$~\citep{gelfond_authorization_2008}.
    \end{itemize}
\end{definition}

However, in order to evaluate any of the above statements, an agent needs to first compute $\delta(V_{lim})$.
To do so, \citet{gelfond_authorization_2008} rely on converting an action description in $\mathcal{AL}$ to an ASP logic program and joining it to $lp(P, s)$.
They extend $lp$ to handle static laws from $\mathcal{AL}$ as follows:
\begin{equation}
    lp(V(L,s)=y \textbf{ if } F) =_{def} val(l, y) \leftarrow lp(F).
\end{equation}

Let $SL$ be the set of all static laws from the action description.

Let $D = \{lp(V(f,s)=y_1) \lor lp(V(f,s)=y_2) \lor \dots \lor lp(V(f,s)=y_n) | f \in \boldsymbol{F}\}$, where $y_1, y_2, \dots, y_n \in \boldsymbol{V}$.

\begin{definition}
    $s \in \delta(V_{lim})$ iff lp(s) is an answer set of $lp(V_{lim}) \cup D \cup lp(SL)$~\citep{gelfond_authorization_2008}.
\end{definition}

\begin{definition}
    \label{def:authorization_event_compliance_partial_knowledge}
    ~

    \begin{itemize}
        \item For all states $s_{possible} \in \delta(V_{lim})$, the event $<s_{possible}, a>$ is \textit{strongly compliant} with $P$ iff the following program is inconsistent~\citep{gelfond_authorization_2008}:
            \begin{equation}
                \label{eq:authorization_partial_knowledge_strongly_compliant}
                lp(P, s_{possible}) \cup D \cup lp(SL) \cup \{ \leftarrow permitted(a). \}
            \end{equation}
        \item For all states $s_{possible} \in \delta(V_{lim})$, the event $<s_{possible}, a>$ is \textit{weakly compliant} with $P$ if $P$ is categorical and the following program is inconsistent~\citep{gelfond_authorization_2008}:
            \begin{equation}
                \label{eq:authorization_partial_knowledge_weakly_compliant}
                lp(P, s_{possible}) \cup D \cup lp(SL) \cup \{ \leftarrow \textbf{not } \neg permitted(a). \}
            \end{equation}
        \item For all states $s_{possible} \in \delta(V_{lim})$, the event $<s_{possible}, a>$ is \textit{non-compliant} with $P$ iff the following program is inconsistent~\citep{gelfond_authorization_2008}:
            \begin{equation}
                \label{eq:authorization_partial_knowledge_non_compliant}
                lp(P, s_{possible}) \cup D \cup lp(SL) \cup \{ \leftarrow \neg permitted(a). \}
            \end{equation}
        \item If $P$ is categorical, then an event $<s_{possible}, a>$, where $s_{possible} \in \delta(V_{lim})$, is also \textit{non-compliant} with $P$ if the program \cref{eq:authorization_partial_knowledge_weakly_compliant} is consistent~\citep{gelfond_authorization_2008}.
    \end{itemize}
\end{definition}

Continuing the running example, suppose an agent did not know that $c_1$ is a colonel.
Then:
\begin{gather}
    \boldsymbol{F_{lim}} = \{ authorized(c_1, m1), commands(c_1, m_1), observer(c_1) \} \\
    V_{lim} = \{ V(authorized(c_1, m1), s) = t, V(commands(c_1, m_1), s) = t, V(observer(c_1), s) = t \}
\end{gather}

$D$ is as follows:
\lstinputlisting[language=Prolog]{Figures/APL_Language/03_Partial_Knowledge/D.lp}

The static laws from the action description in \cref{eq:apl_action_description_1,eq:apl_action_description_2,eq:apl_action_description_3,eq:apl_action_description_4,eq:apl_action_description_5} can be encoded as follows:
\lstinputlisting[language=Prolog]{Figures/APL_Language/03_Partial_Knowledge/SL.lp}

The encoding of $\delta(V_{lim})$ is as follows:
\lstinputlisting[language=Prolog]{Figures/APL_Language/03_Partial_Knowledge/delta_V_lim.lp}

%\lstinputlisting[language=Prolog]{Figures/APL_Language/03_Partial_Knowledge/Pm.lp}

The result of logic program \cref{eq:authorization_partial_knowledge_strongly_compliant} is as follows:
\lstinputlisting{Figures/APL_Language/03_Partial_Knowledge/strongly_compliant.snippet.txt}
Thus $A$ is not strongly compliant with $P_m$.

The result of logic program \cref{eq:authorization_partial_knowledge_weakly_compliant} is as follows:
\lstinputlisting{Figures/APL_Language/03_Partial_Knowledge/weakly_compliant.snippet.txt}
Thus $A$ is not weakly compliant with $P_m$.

The result of logic program \cref{eq:authorization_partial_knowledge_strongly_compliant} is as follows:
\lstinputlisting{Figures/APL_Language/03_Partial_Knowledge/non_compliant.snippet.txt}
However, before we can say $A$ is non-compliant, we must first determine whether $P_m$ is categorical.
To do so, we will rerun the second test giving \textsc{clingo} the option ``0'', which instructs \textsc{clingo} to find every satisfiable model instead of stopping at the first one.
The output of \cref{eq:authorization_partial_knowledge_weakly_compliant} with option ``0'' is as follows:
\lstinputlisting{Figures/APL_Language/03_Partial_Knowledge/non_compliant_categorical.snippet.txt}
Since ``Models: 1'', $P_m$ is categorical and hence $A$ is non-compliant.

\subsection{Checking Compliance using a History of Past Actions}

\Citet{gelfond_authorization_2008} note that the above two approaches will not work for agents in the AAA architecture (or for that matter, the $\mathcal{AIA}$ architecture).
Nonetheless, they develop a method for checking policy compliance under the AAA architecture knowledge constraints.
As mentioned previously, agents in the AAA architecture have an initial description of their initial state (complete or partial), a history of their actions $A_0, A_1, \dots, A_n$, and may have memory of fluent values at particular points in time.

\Citet{gelfond_authorization_2008} define a new function $lp(P, I)$ that adds a step $I$ argument to each ASP atom from $lp(P)$ (\cref{eq:apl_lp_01,eq:apl_lp_02,eq:apl_lp_03,eq:apl_lp_04,eq:apl_lp_05,eq:apl_lp_06,eq:apl_lp_07,eq:apl_lp_08,eq:apl_lp_09,eq:apl_lp_10}).
The new $lp$ function is defined as follows:
\begin{gather}
    lp(P,s,I) =_{def} lp(P,I)\cup lp(s,I). \label{eq:apl_lp_inc_01} \\
    lp(P,I)=_{def}\{lp(statement,I) | statement \in P\} \label{eq:apl_lp_inc_02} \\
    lp\left(V(f,s)=y,I\right) =_{def}
        val\left(f,y,I\right). \label{eq:apl_lp_inc_03} \\
    lp(permitted(a),I) =_{def}
        permitted(a, I). \label{eq:apl_lp_inc_04} \\
    lp(\neg permitted(a),I) =_{def}
        \neg permitted(a, I). \label{eq:apl_lp_inc_05} \\
    lp(permitted(a) \textbf{ if } F, I) =_{def}
        permitted(a, I) \leftarrow
            lp(F, I). \label{eq:apl_lp_inc_06} \\
    lp(\neg permitted(a) \textbf{ if } F, I) =_{def}
        \neg permitted(a, I) \leftarrow
            lp(F, I). \label{eq:apl_lp_inc_07}
\end{gather}
\begin{multline}
    \label{eq:apl_lp_inc_08}
    lp(d_i: \textbf{normally } permitted(a) \textbf{ if } F_i, I) =_{def} \\
        permitted(a, I) \leftarrow
            lp(F, I),
            \textbf{not } ab(d, I),
            \textbf{not } \neg permitted(a, I).
\end{multline}
\begin{multline}
    \label{eq:apl_lp_inc_09}
    lp(d_j: \textbf{normally } \neg permitted(a) \textbf{ if } F_j, I) =_{def} \\
        \neg permitted(a, I) \leftarrow
            lp(F, I),
            \textbf{not } ab(d, I),
            \textbf{not } permitted(a, I).
\end{multline}
\begin{gather}
    lp(prefer(d_i, d_j), I) =_{def}
        ab(d_j, I) \leftarrow lp(F_1, I). \label{eq:apl_lp_inc_10}
\end{gather}

For a history $H_n=<s, A_0, A_1, \dots, A_{n-1}>$, they also add:
\begin{gather}
    lp(a, I)=_{def}occurs(a, I). \\
    lp(P, H) =_{def} lp(P, I) \cup lp(s, 0) \cup lp(A_0, 0) \cup lp(A_1, 1) \cup \dots \cup lp(A_{i-1}, I-1)
\end{gather}

Similar to before, the notation of \citet{gelfond_authorization_2008} will be simplified when $\boldsymbol{V}=\{t,f\}$ so that \cref{eq:apl_lp_inc_03} will represented as:
\begin{gather}
    lp\left(V(f,s)=t,I\right) =_{def}
        val\left(f,I\right). \\
    lp\left(V(f,s)=f,I\right) =_{def}
        -val\left(f,I\right). \\
\end{gather}

Let $D_0 = \{lp(V(f,s)=y_1, 0) \lor lp(V(f,s)=y_2, 0) \lor \dots \lor lp(V(f,s)=y_n, 0) | f \in \boldsymbol{F}\}$, where $y_1, y_2, \dots, y_n \in \boldsymbol{V}$

\begin{definition}
    \label{def:model_of_history}
    A trajectory\footnotemark $s_0, A_0, s_1, A_1, \dots, A_{n-1}, s_n$ of a transition system $T$ is a \textit{model} of a history $H_n = <s, A_0, A_1, \dots, A_{n-1}>$ if $s \subseteq s_0$~\citep{gelfond_authorization_2008}.
    Thus, a trajectory that meets this condition does not contradict what has been recorded in the agent's history.
    \footnotetext{
        \citet{gelfond_authorization_2008} define `trajectory' and `history' in terms of elementary actions in contrast to \citet{blount_architecture_2013,gelfond_action_1998}.
        In order to maintain continuity with the definition of action signatures and transition systems presented in \cref{def:transition_system}, \cref{def:model_of_history} has been extended to include compound actions.
    }
\end{definition}

Let $Check_1(H_n)$ be the following subprogram:
\begin{gather}
    \neg strongly\_compliant \leftarrow occurs(A, I), \textbf{ not } permitted(A, I). \\
    \leftarrow \textbf{not } \neg strongly\_compliant.
\end{gather}

Let $Check_2(H_n)$ be the following subprogram:
\begin{gather}
    \neg weakly\_compliant \leftarrow occurs(A, I), \neg permitted(A, I). \\
    \leftarrow \textbf{not } \neg weakly\_compliant.
\end{gather}

\begin{definition}
    \label{def:authorization_event_compliance_history_knowledge}
    ~

    \begin{itemize}
        \item A model of $H_n$ is \textit{strongly compliant} with $P$ iff the following program is inconsistent~\citep{gelfond_authorization_2008}:
            \begin{equation}
                \label{eq:authorization_history_knowledge_strongly_compliant}
                lp(P, H_n) \cup D \cup lp(A) \cup Check_1
            \end{equation}
        \item A model of $H_n$ is \textit{weakly compliant} with $P$ iff the following program is inconsistent~\citep{gelfond_authorization_2008}:
            \begin{equation}
                \label{eq:authorization_history_knowledge_weakly_compliant}
                lp(P, H_n) \cup D \cup lp(A) \cup Check_2
            \end{equation}
        \item A model of $H_n$ that is neither strongly compliant nor weakly compliant must be non-compliant~\citep{gelfond_authorization_2008}.
    \end{itemize}
\end{definition}

Continuing the previous example, the logic encoding of $P_m$ with the new $lp$ function is as follows:
\lstinputlisting[language=Prolog]{Figures/APL_Language/04_History_Knowledge/Pm.lp}

$D_0$ is as follows:
\lstinputlisting[language=Prolog]{Figures/APL_Language/04_History_Knowledge/D0.lp}

Suppose our agent has the following history\footnotemark:
\begin{gather}
    s_0 = \{ colonel(c_1), -observer(c_1), -authorized(c_1, m_1), -commands(c_1, m_1) \} \\
    A_0 = \{ authorize(c_1, m_1) \} \\
    H_1 = < s_0, A_0 >
\end{gather}
and is considering executing action $A_1 = \{ assume\_command(c_1, m_1) \}$ in the current time step 1.
Before executing $A_1$, our agent first wants to verify its compliance.

\footnotetext{
    \citet{gelfond_authorization_2008} use a different notation for describing an agent's history than what was introduced in \cref{subsubsec:domain_history} while describing the work of \citet{blount_towards_2014}.
}

The logic encoding $lp(H_1)$ would be as follows\footnotemark:
\footnotetext{
    It is worth noting that the syntax \citet{gelfond_authorization_2008} provide for $lp(H_1)$ is very similar to that introduced in \citet{blount_towards_2014}.
    Whereas \citet{blount_towards_2014} would write $holds(FluentName, FluentValue, Timestep)$ and $occurs(ActionName, Timestep)$,\citet{gelfond_authorization_2008} write $val(FluentName, FluentValue, Timestep)$ and $occurs(ActionName, Timestep)$, respectively.
    % TODO: Review distinction between obs and holds, hpd and occurs.
    %   See section 10.5 of "Knowledge Representation, Reasoning, and the Design of Intelligent Agents: The Answer-Set Programming Approach" by Gelfond and Kahl, 2014
}

\lstinputlisting[language=Prolog]{Figures/APL_Language/04_History_Knowledge/Hn.lp}

The logic encoding $lp(A_1)$ is as follows:
\lstinputlisting[language=Prolog]{Figures/APL_Language/04_History_Knowledge/A1.lp}

$Check_1$ and $Check_2$ are as follows:
\lstinputlisting[language=Prolog]{Figures/APL_Language/04_History_Knowledge/Check_1.lp}
\lstinputlisting[language=Prolog]{Figures/APL_Language/04_History_Knowledge/Check_2.lp}

The output of logic program \cref{eq:authorization_history_knowledge_strongly_compliant} is as follows:
\lstinputlisting{Figures/APL_Language/04_History_Knowledge/Check_1.snippet.txt}

The output of logic program \cref{eq:authorization_history_knowledge_weakly_compliant} is as follows:
\lstinputlisting{Figures/APL_Language/04_History_Knowledge/Check_2.snippet.txt}

By this output, we might be led to say that action $A_1$ is weakly compliant given history $H_n$.
However, \citet{gelfond_authorization_2008} implicitly assume that the inertia property of inertial fluents is already encoded in logic form.
To do this, we must add the following \textit{inertia axiom} for the inertial fluents $authorized(C,M),\ commands(C,M),\ colonel(C),\ observer(C)$:
\lstinputlisting[language=Prolog]{Figures/APL_Language/04_History_Knowledge/Inertia_Axiom.lp}

With this inertia axiom, \cref{eq:authorization_history_knowledge_strongly_compliant} is inconsistent hence action $A_1$ is strongly compliant given history $H_n$.

\lstinputlisting{Figures/APL_Language/04_History_Knowledge/Check_1_with_Inertia_Axiom.snippet.txt}
\lstinputlisting{Figures/APL_Language/04_History_Knowledge/Check_2_with_Inertia_Axiom.snippet.txt}
