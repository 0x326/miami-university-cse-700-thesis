[APIA]: https://gitlab.com/0x326/miami-university-cse-700-apia.git

# The Architecture for Policy-Aware Intentional Agents (APIA)

This is the version of APIA as of the publication of this thesis.
Any future development is directed to [this repo][APIA].

## Dependencies

- At least Python 3.9
- At least clingo 5.4.1

## Usage

Running:

```bash
./apia_control_loop.py
```

Debugging:

```bash
./test.sh  # See "Usage:" for arguments

./diff_test.sh  # See "Usage:" for arguments

./walk_through_test.sh  # See "Usage:" for arguments
```
